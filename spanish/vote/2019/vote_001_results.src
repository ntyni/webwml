#use wml::debian::translation-check translation="5127161ff176cab5734ac40fd7c6f6ccf12526f4"
           <p class="center">
             <a style="margin-left: auto; margin-right: auto;" href="vote_001_results.dot">
               <img src="vote_001_results.png" alt="Representación gráfica de los resultados">
               </a>
           </p>
             <p>
               En el gráfico anterior los nodos rosa indican que
               esa opción no ha superado la mayoría y la azul es la opción
               ganadora. El octógono se utiliza para las opciones que no
               han batido a la opción por omisión.  
           </p>
           <ul>
<li>Opción 1: "Joerg Jaspert"</li>
<li>Opción 2: "Jonathan Carter"</li>
<li>Opción 3: "Sam Hartman"</li>
<li>Opción 4: "Martin Michlmayr"</li>
<li>Opción 5: "None Of The Above"</li>
           </ul>
            <p>
               En la tabla siguiente, escrutinio[fila x][columna y] representa
               el número de votos en los que se prefiere la opción x sobre la opción y. Una 
               <a href="https://en.wikipedia.org/wiki/Schwartz_method">explicación
                 más detallada de la matriz de duelos</a> puede ayudar a
               entender la tabla. Para entender el método Condorcet, la
               <a href="https://en.wikipedia.org/wiki/Condorcet_method">entrada de la
		 Wikipedia</a> es bastante informativa.
           </p>
           <table class="vote">
             <caption class="center"><strong>Matriz de duelos</strong></caption>
	     <tr><th>&nbsp;</th><th colspan="5" class="center">Opción</th></tr>
              <tr>
                   <th>&nbsp;</th>
                   <th>    1 </th>
                   <th>    2 </th>
                   <th>    3 </th>
                   <th>    4 </th>
                   <th>    5 </th>
              </tr>
                 <tr>
                   <th>Opción 1  </th>
                   <td>&nbsp;</td>
                   <td>  169 </td>
                   <td>  128 </td>
                   <td>  169 </td>
                   <td>  319 </td>
                 </tr>
                 <tr>
                   <th>Opción 2  </th>
                   <td>  166 </td>
                   <td>&nbsp;</td>
                   <td>  109 </td>
                   <td>  172 </td>
                   <td>  323 </td>
                 </tr>
                 <tr>
                   <th>Opción 3  </th>
                   <td>  217 </td>
                   <td>  206 </td>
                   <td>&nbsp;</td>
                   <td>  202 </td>
                   <td>  336 </td>
                 </tr>
                 <tr>
                   <th>Opción 4  </th>
                   <td>  180 </td>
                   <td>  174 </td>
                   <td>  134 </td>
                   <td>&nbsp;</td>
                   <td>  324 </td>
                 </tr>
                 <tr>
                   <th>Opción 5  </th>
                   <td>   51 </td>
                   <td>   37 </td>
                   <td>   26 </td>
                   <td>   35 </td>
                   <td>&nbsp;</td>
                 </tr>
               </table>
              <p>

Mirando a la fila 2, columna 1, en 166 votos<br/>
se prefirió a Jonathan Carter sobre Joerg Jaspert.<br/>
<br/>
Mirando a la fila 1, columna 2, en 169 votos<br/>
se prefirió a Joerg Jaspert sobre Jonathan Carter.<br/>
              <h3>Enfrentamientos por parejas</h3>
              <ul>
                <li>La opción 1 vence a la opción 2 por (169 - 166) =    3 votos.</li>
                <li>La opción 3 vence a la opción 1 por (217 - 128) =   89 votos.</li>
                <li>La opción 4 vence a la opción 1 por (180 - 169) =   11 votos.</li>
                <li>La opción 1 vence a la opción 5 por (319 -  51) =  268 votos.</li>
                <li>La opción 3 vence a la opción 2 por (206 - 109) =   97 votos.</li>
                <li>La opción 4 vence a la opción 2 por (174 - 172) =    2 votos.</li>
                <li>La opción 2 vence a la opción 5 por (323 -  37) =  286 votos.</li>
                <li>La opción 3 vence a la opción 4 por (202 - 134) =   68 votos.</li>
                <li>La opción 3 vence a la opción 5 por (336 -  26) =  310 votos.</li>
                <li>La opción 4 vence a la opción 5 por (324 -  35) =  289 votos.</li>
              </ul>
              <h3>El conjunto de Schwartz contiene</h3>
              <ul>
                <li>La opción 3: "Sam Hartman"</li>
              </ul>
              <h3>Los ganadores</h3>
              <ul>
                <li>La opción 3: "Sam Hartman"</li>
              </ul>
              <p>
               Debian utiliza el método Condorcet en las votaciones.
               Esquemáticamente, el método Condorcets 
               se puede enunciar de la siguiente manera: <br/>
               <q>Considérense todos los posibles enfrentamientos entre pares de candidatos.
                  El ganador Condorcet, si es que hay alguno, es el
                  candidato que puede vencer a todos los demás en dichos enfrentamientos
                  entre pares</q>.
               El problema es que, en votaciones complejas, puede
               haber un resultado circular en el que A vence a B, B vence a C
               y C vence a A. Las distintas variaciones de Condorcet usan
               diferentes maneras de resolver el empate. Consulte
               <a href="https://en.wikipedia.org/wiki/Cloneproof_Schwartz_Sequential_Dropping">Cloneproof Schwartz Sequential Dropping («el método Schulze»)</a>
               para más detalles. La variación de Debian está articulada en la
               <a href="$(HOME)/devel/constitution">constitución</a>,
               en concreto, en A.6.
              </p>
