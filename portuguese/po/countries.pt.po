# Brazilian Portuguese translation for Debian website countries.pot
# Copyright (C) 2003-2017 Software in the Public Interest, Inc.
#
# Michelle Ribeiro <michelle@cipsga.org.br>, 2003
# Gustavo R. Montesino <grmontesino@ig.com.br>, 2004
# Felipe Augusto van de Wiel (faw) <faw@debian.org>, 2006-2008
# Marcelo Gomes de Santana <marcelo@msantana.eng.br>, 2011-2017.
#
msgid ""
msgstr ""
"Project-Id-Version: Debian Webwml\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2022-02-04 16:01-0300\n"
"Last-Translator: Thiago Pezzo (tico) <pezzo@protonmail.com>\n"
"Language-Team: Brazilian Portuguese <debian-l10n-portuguese@lists.debian."
"org>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.4.2\n"

#: ../../english/template/debian/countries.wml:111
msgid "United Arab Emirates"
msgstr "Emirados Árabes Unidos"

#: ../../english/template/debian/countries.wml:114
msgid "Albania"
msgstr "Albânia"

#: ../../english/template/debian/countries.wml:117
msgid "Armenia"
msgstr "Armênia"

#: ../../english/template/debian/countries.wml:120
msgid "Argentina"
msgstr "Argentina"

#: ../../english/template/debian/countries.wml:123
msgid "Austria"
msgstr "Áustria"

#: ../../english/template/debian/countries.wml:126
msgid "Australia"
msgstr "Austrália"

#: ../../english/template/debian/countries.wml:129
msgid "Bosnia and Herzegovina"
msgstr "Bósnia e Herzegovina"

#: ../../english/template/debian/countries.wml:132
msgid "Bangladesh"
msgstr "Bangladesh"

#: ../../english/template/debian/countries.wml:135
msgid "Belgium"
msgstr "Bélgica"

#: ../../english/template/debian/countries.wml:138
msgid "Bulgaria"
msgstr "Bulgária"

#: ../../english/template/debian/countries.wml:141
msgid "Brazil"
msgstr "Brasil"

#: ../../english/template/debian/countries.wml:144
msgid "Bahamas"
msgstr "Bahamas"

#: ../../english/template/debian/countries.wml:147
msgid "Belarus"
msgstr "Bielorrússia"

#: ../../english/template/debian/countries.wml:150
msgid "Canada"
msgstr "Canadá"

#: ../../english/template/debian/countries.wml:153
msgid "Switzerland"
msgstr "Suíça"

#: ../../english/template/debian/countries.wml:156
msgid "Chile"
msgstr "Chile"

#: ../../english/template/debian/countries.wml:159
msgid "China"
msgstr "China"

#: ../../english/template/debian/countries.wml:162
msgid "Colombia"
msgstr "Colômbia"

#: ../../english/template/debian/countries.wml:165
msgid "Costa Rica"
msgstr "Costa Rica"

#: ../../english/template/debian/countries.wml:168
msgid "Czech Republic"
msgstr "República Checa"

#: ../../english/template/debian/countries.wml:171
msgid "Germany"
msgstr "Alemanha"

#: ../../english/template/debian/countries.wml:174
msgid "Denmark"
msgstr "Dinamarca"

#: ../../english/template/debian/countries.wml:177
msgid "Dominican Republic"
msgstr "República Dominicana"

#: ../../english/template/debian/countries.wml:180
msgid "Algeria"
msgstr "Argélia"

#: ../../english/template/debian/countries.wml:183
msgid "Ecuador"
msgstr "Equador"

#: ../../english/template/debian/countries.wml:186
msgid "Estonia"
msgstr "Estônia"

#: ../../english/template/debian/countries.wml:189
msgid "Egypt"
msgstr "Egito"

#: ../../english/template/debian/countries.wml:192
msgid "Spain"
msgstr "Espanha"

#: ../../english/template/debian/countries.wml:195
msgid "Ethiopia"
msgstr "Etiópia"

#: ../../english/template/debian/countries.wml:198
msgid "Finland"
msgstr "Finlândia"

#: ../../english/template/debian/countries.wml:201
msgid "Faroe Islands"
msgstr "Ilhas Feroe"

#: ../../english/template/debian/countries.wml:204
msgid "France"
msgstr "França"

#: ../../english/template/debian/countries.wml:207
msgid "United Kingdom"
msgstr "Reino Unido"

#: ../../english/template/debian/countries.wml:210
msgid "Grenada"
msgstr "Granada"

#: ../../english/template/debian/countries.wml:213
msgid "Georgia"
msgstr "Geórgia"

#: ../../english/template/debian/countries.wml:216
msgid "Greenland"
msgstr "Groenlândia"

#: ../../english/template/debian/countries.wml:219
msgid "Greece"
msgstr "Grécia"

#: ../../english/template/debian/countries.wml:222
msgid "Guatemala"
msgstr "Guatemala"

#: ../../english/template/debian/countries.wml:225
msgid "Hong Kong"
msgstr "Hong Kong"

#: ../../english/template/debian/countries.wml:228
msgid "Honduras"
msgstr "Honduras"

#: ../../english/template/debian/countries.wml:231
msgid "Croatia"
msgstr "Croácia"

#: ../../english/template/debian/countries.wml:234
msgid "Hungary"
msgstr "Hungria"

#: ../../english/template/debian/countries.wml:237
msgid "Indonesia"
msgstr "Indonésia"

#: ../../english/template/debian/countries.wml:240
msgid "Iran"
msgstr "Irã"

#: ../../english/template/debian/countries.wml:243
msgid "Ireland"
msgstr "Irlanda"

#: ../../english/template/debian/countries.wml:246
msgid "Israel"
msgstr "Israel"

#: ../../english/template/debian/countries.wml:249
msgid "India"
msgstr "Índia"

#: ../../english/template/debian/countries.wml:252
msgid "Iceland"
msgstr "Islândia"

#: ../../english/template/debian/countries.wml:255
msgid "Italy"
msgstr "Itália"

#: ../../english/template/debian/countries.wml:258
msgid "Jordan"
msgstr "Jordânia"

#: ../../english/template/debian/countries.wml:261
msgid "Japan"
msgstr "Japão"

#: ../../english/template/debian/countries.wml:264
msgid "Kenya"
msgstr "Quênia"

#: ../../english/template/debian/countries.wml:267
msgid "Kyrgyzstan"
msgstr "Cazaquistão"

#: ../../english/template/debian/countries.wml:270
msgid "Cambodia"
msgstr "Camboja"

#: ../../english/template/debian/countries.wml:273
msgid "Korea"
msgstr "Coreia"

#: ../../english/template/debian/countries.wml:276
msgid "Kuwait"
msgstr "Kuwait"

#: ../../english/template/debian/countries.wml:279
msgid "Kazakhstan"
msgstr "Cazaquistão"

#: ../../english/template/debian/countries.wml:282
msgid "Sri Lanka"
msgstr "Sri Lanka"

#: ../../english/template/debian/countries.wml:285
msgid "Lithuania"
msgstr "Lituânia"

#: ../../english/template/debian/countries.wml:288
msgid "Luxembourg"
msgstr "Luxemburgo"

#: ../../english/template/debian/countries.wml:291
msgid "Latvia"
msgstr "Letônia"

#: ../../english/template/debian/countries.wml:294
msgid "Morocco"
msgstr "Marrocos"

#: ../../english/template/debian/countries.wml:297
msgid "Monaco"
msgstr "Mônaco"

#: ../../english/template/debian/countries.wml:300
msgid "Moldova"
msgstr "Moldávia"

#: ../../english/template/debian/countries.wml:303
msgid "Montenegro"
msgstr "Montenegro"

#: ../../english/template/debian/countries.wml:306
msgid "Madagascar"
msgstr "Madagáscar"

#: ../../english/template/debian/countries.wml:309
msgid "Macedonia, Republic of"
msgstr "República da Macedônia"

#: ../../english/template/debian/countries.wml:312
msgid "Mongolia"
msgstr "Mongólia"

#: ../../english/template/debian/countries.wml:315
msgid "Malta"
msgstr "Malta"

#: ../../english/template/debian/countries.wml:318
msgid "Mexico"
msgstr "México"

#: ../../english/template/debian/countries.wml:321
msgid "Malaysia"
msgstr "Malásia"

#: ../../english/template/debian/countries.wml:324
msgid "New Caledonia"
msgstr "Nova Caledônia"

#: ../../english/template/debian/countries.wml:327
msgid "Nicaragua"
msgstr "Nicarágua"

#: ../../english/template/debian/countries.wml:330
msgid "Netherlands"
msgstr "Holanda"

#: ../../english/template/debian/countries.wml:333
msgid "Norway"
msgstr "Noruega"

#: ../../english/template/debian/countries.wml:336
msgid "New Zealand"
msgstr "Nova Zelândia"

#: ../../english/template/debian/countries.wml:339
msgid "Panama"
msgstr "Panamá"

#: ../../english/template/debian/countries.wml:342
msgid "Peru"
msgstr "Peru"

#: ../../english/template/debian/countries.wml:345
msgid "French Polynesia"
msgstr "Polinésia Francesa"

#: ../../english/template/debian/countries.wml:348
msgid "Philippines"
msgstr "Filipinas"

#: ../../english/template/debian/countries.wml:351
msgid "Pakistan"
msgstr "Paquistão"

#: ../../english/template/debian/countries.wml:354
msgid "Poland"
msgstr "Polônia"

#: ../../english/template/debian/countries.wml:357
msgid "Portugal"
msgstr "Portugal"

#: ../../english/template/debian/countries.wml:360
msgid "Réunion"
msgstr "Reunião"

#: ../../english/template/debian/countries.wml:363
msgid "Romania"
msgstr "Romênia"

#: ../../english/template/debian/countries.wml:366
msgid "Serbia"
msgstr "Sérvia"

#: ../../english/template/debian/countries.wml:369
msgid "Russia"
msgstr "Rússia"

#: ../../english/template/debian/countries.wml:372
msgid "Saudi Arabia"
msgstr "Arábia Saudita"

#: ../../english/template/debian/countries.wml:375
msgid "Sweden"
msgstr "Suécia"

#: ../../english/template/debian/countries.wml:378
msgid "Singapore"
msgstr "Singapura"

#: ../../english/template/debian/countries.wml:381
msgid "Slovenia"
msgstr "Eslovênia"

#: ../../english/template/debian/countries.wml:384
msgid "Slovakia"
msgstr "Eslováquia"

#: ../../english/template/debian/countries.wml:387
msgid "El Salvador"
msgstr "Salvador"

#: ../../english/template/debian/countries.wml:390
msgid "Thailand"
msgstr "Tailândia"

#: ../../english/template/debian/countries.wml:393
msgid "Tajikistan"
msgstr "Tajiquistão"

#: ../../english/template/debian/countries.wml:396
msgid "Tunisia"
msgstr "Tunísia"

#: ../../english/template/debian/countries.wml:399
msgid "Turkey"
msgstr "Turquia"

#: ../../english/template/debian/countries.wml:402
msgid "Taiwan"
msgstr "Taiwan"

#: ../../english/template/debian/countries.wml:405
msgid "Ukraine"
msgstr "Ucrânia"

#: ../../english/template/debian/countries.wml:408
msgid "United States"
msgstr "Estados Unidos"

#: ../../english/template/debian/countries.wml:411
msgid "Uruguay"
msgstr "Uruguai"

#: ../../english/template/debian/countries.wml:414
msgid "Uzbekistan"
msgstr "Uzbequistão"

#: ../../english/template/debian/countries.wml:417
msgid "Venezuela"
msgstr "Venezuela"

#: ../../english/template/debian/countries.wml:420
msgid "Vietnam"
msgstr "Vietnã"

#: ../../english/template/debian/countries.wml:423
msgid "Vanuatu"
msgstr "Vanuatu"

#: ../../english/template/debian/countries.wml:426
msgid "South Africa"
msgstr "África do Sul"

#: ../../english/template/debian/countries.wml:429
msgid "Zimbabwe"
msgstr "Zimbábue"

#~ msgid "Great Britain"
#~ msgstr "Reino Unido"
