#use wml::debian::template title="Programa de parceiros(as) do Debian 2017 - descrição"
#use wml::debian::translation-check translation="f7210a80d7468806cac65059e70b3f611bfaab48"

<h3>O que é o programa de parceiros(as) do Debian?</h3>
<div class="centerblock">
  <p>O programa de parceiros(as) foi criado para reconhecer empresas e
    organizações que fornecem assistência contínua ao projeto Debian. Devido ao
    suporte substancial fornecido por esses(as) parceiros(as), somos capazes de
    expandir e melhorar o Debian. Gostaríamos de reconhecer oficialmente seus
    esforços e continuar mantendo um bom relacionamento de trabalho.</p>
</div>

<h3>Quais são os critérios para os(as) parceiros(as)?</h3>
  Existem vários tipos de parcerias.

<ul>
<li><b>Parceiros(as) de desenvolvimento</b><br>
  <ul>
  <li>fornecer equipamento ao Debian (a um custo bastante reduzido ou por
      empréstimo a longo prazo).
  <li>fornecer equipamentos para uso em conferências, feiras, etc.
  <li>fornecer um serviço baseado em hardware ao Debian (acesso à internet,
      contas, etc).
  <li>fornecedores comerciais que mantêm pacotes-chave no Debian.
  <li>ajudar ou liderar esforços para portar e manter pacotes do Debian para
      seus hardwares.
  <li>fornecer serviço técnico ou consultoria gratuita ou com custo reduzido
      ao Debian.
  </ul>
<li><b>Parceiros(as) de serviços</b><br>
  <ul>
  <li>ajudar o Debian na divulgação de lançamentos.
  <li>fornecer um fórum para as notícias de lançamentos e anúncios do Debian.
  <li>fornecer um serviço não técnico gratuito ou com custo reduzido ao Debian.
  </ul>
<li><b>Parceiros(as) financeiros(as)</b><br>
 <ul>
 <li>fornecer suporte financeiro contínuo, como programas de compartilhamento
     de receita.
 </ul>
</ul>

<p>
Para ser um(a) parceiro(a), pelo menos um dos critérios acima deve ser atendido.
Os(As) parceiros(as) que se enquadram em mais de uma categoria podem ser
listados(as) em cada categoria para a qual se qualificam.

<h3>O que o Debian faz pelos(as) parceiros(as)?</h3>
O Debian trabalhará em estreita colaboração com todos os(as) parceiros(as) para
garantir que o Debian entenda as necessidades e preocupações dos(as)
parceiros(as) e vice-versa. Isso permitirá que o Debian permaneça adequadamente
focado no mercado através do feedback dos(as) parceiros(as).

<p>
Cada parceiro(a) terá espaço na página web de parceiros(as) do Debian daquele
ano, descrevendo a natureza da parceria.
O(A) parceiro(a) pode fornecer links para serem incluídos nessas informações.

<!--
<p>
Para empresas que fornecem hardware para projetos de portabilidade, esta página
também será vinculada diretamente a partir das páginas de portabilidade
relevantes.
  -->

<h3>O que os(as) parceiros(as) fazem pelo Debian</h3>
Além de atender aos critérios de parceria mencionados acima, os(as)
parceiros(as) devem mencionar o Debian com destaque nas áreas atendidas pela
parceria.
Eles(as) devem trabalhar com o Debian para promover os interesses do projeto.
As possibilidades incluem:
<ul>
<li> Links para as páginas web relevantes do Debian.
<li> Publicidade por meio de banners/botons/anúncios.
<li> Links para atuais notícias/eventos/projetos do Debian.
<li> ...e muito mais!
</ul>


<h3>Como um(a) fornecedor(a) se torna um(a) parceiro(a)?</h3>

Você deve enviar um e-mail para
<a href="mailto:partners@debian.org">partners@debian.org</a>
com a <a href="../partners-form">proposta</a> de parceria.
Esta proposta será revisada pelo Debian e discutida com o(a) possível
parceiro(a). O Debian então irá decidir se uma parceria é possível ou não.


<h3>Como e quando terminam as parcerias?</h3>
O Debian ou o(a) parceiro(a) pode optar por encerrar a parceria a qualquer
momento.
Quando a parceria termina, as entradas na página web relacionadas ao(a)
parceiro(a) são removidas da listagem atual (as entradas nos anos anteriores
permanecem).
As parcerias podem terminar por vários motivos:
<ul>
<li>o equipamento emprestado é devolvido.
<li>o serviço não é mais fornecido.
<li>os objetivos dos(as) parceiros(as) não coincidem mais com os objetivos do
Debian.
</ul>

<h3>Existem outras maneiras pelas quais minha organização pode contribuir com
o Debian?</h3>
<div class="centerblock">
<p>
Para doações de dinheiro, equipamentos, ou serviços, visite nossa página de
<a href="../../donations">doações</a>.</p>

<p>
O patrocínio da DebConf (a conferência anual do Debian) é gerenciado pela equipe
da organização da DebConf de cada ano.
Para patrocínio da DebConf, viste o site web da
<a href="https://www.debconf.org">DebConf</a>.</p>

<p>
O financiamento do Debian LTS é gerenciado pela equipe do LTS.
Para financiamento do LTS, visite a página wiki do
<a href="https://wiki.debian.org/LTS">LTS</a>.</p>

<p>
Se você tem outra ideia que não se encaixa nas sugestões acima,
<a href="../../contact">entre com contato conosco</a>.
</p>
</div>
