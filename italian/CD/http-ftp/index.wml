#use wml::debian::cdimage title="Scaricare le immagini dei CD/DVD Debian via HTTP/FTP" BARETITLE=true
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/images.data"
#use wml::debian::translation-check translation="3d70cb0d8821c3f8ec922ac96298ad77e041fc7e" maintainer="Luca Monducci"

<div class="tip">
<p><strong>Si raccomanda di non scaricare le immagini dei CD o dei DVD
con il proprio browser, così come si fa con gli altri file.</strong> Se il
download si interrompe, la maggior parte dei browser non è in grado
di riprenderlo dal punto al quale era arrivato.</p>
</div>

<p>Si consiglia di utilizzare uno strumento che supporti il <em>resume</em>,
tipicamente questi strumenti sono chiamati <q>download manager</q>. Tale
funzionalità è offerta da molti componenti aggiuntivi per i browser oppure
da dei programmi specifici. Sotto Linux/Unix, è possibile usare <a
href="https://aria2.github.io/">aria2</a>, <a
href="https://sourceforge.net/projects/dfast/">wxDownload Fast</a> oppure (dalla
riga di comando) <q><tt>wget&nbsp;-c&nbsp;</tt><em>URL</em></q> oppure
<q><tt>curl&nbsp;-C&nbsp;-&nbsp;-L&nbsp;-O&nbsp;</tt><em>URL</em></q>.
Molti altri download manager sono elencati in <a
href="https://en.wikipedia.org/wiki/Comparison_of_download_managers">confronto
dei download manager</a>.</p>

<p>Sono disponibili per il download le seguenti immagini Debian:</p>

<ul>

  <li><a href="#stable">Immagini ufficiali dei CD/DVD della distribuzione
  <q>stable</q></a></li>

  <li><a href="#firmware">Immagini <b>non ufficiali</b> di CD/DVD per
  <q>stable</q> con firmware <b>non-free</b> incluso</a></li>

<comment>
  <li>Immagini non ufficiali dei CD/DVD delle distribuzioni <q>testing</q> e
  <q>unstable</q> da fsn://HU - <a href="#unofficial">vedi oltre</a></li>
</comment>

</ul>

<p>Vedere anche:</p>

<ul>

  <li>L'elenco completo dei <a href="#mirrors">mirror di <tt>debian-cd/</tt></a></li>

  <li>Le immagini <q>netinst</q> (installazione via rete da 150-300&nbsp;MB),
  si veda la pagina dell'<a href="../netinst/">installazione
  via rete</a>.</li>

  <li>Le immagini della distribuzione <q>testing</q> nella pagina dell'<a
  href="$(DEVEL)/debian-installer/">Installatore Debian</a>.</li>

</ul>

<hr />

<h2><a name="stable">Immagini ufficiali dei CD/DVD per la distribuzione <q>stable</q></a></h2>

<p>Per installare Debian su una macchina senza una connessione a Internet,
è possibile usare le immagini dei CD (700&nbsp;MB ciascuna) oppure le
immagini dei DVD (4,4&nbsp;GB ciascuna). Scaricare il primo file immagine di
un CD o DVD, scrivere l'immagine su un disco con un masterizzatore CD/DVD
(oppure per le architetture i386 e amd64 su una chiavetta USB)
e poi avviare la macchina con quello.</p>

<p>Il <strong>primo</strong> disco CD/DVD contiene tutti i file necessari
all'installazione di un sistema Debian standard.<br />
Per evitare di scaricare cose inutili, <strong>non</strong> scaricare i
file con le immagini degli altri CD o DVD a meno che non si sia certi che
si ha bisogno dei pacchetti in essi contenuti.</p>

<div class="line">
<div class="item col50">
<p><strong>CD</strong></p>

<p>I seguenti collegamenti puntano ai file con le immagini di dimensione
fino a 650&nbsp;MB, e quindi adatti per essere scritti su un comune supporto
CD-R(W):</p>

<stable-full-cd-images />
</div>
<div class="item col50 lastcol">
<p><strong>DVD</strong></p>

<p>I seguenti collegamenti puntano ai file con le immagini di dimensione
fino a 4,4&nbsp;GB, e quindi adatti per essere incisi su un comune supporto
DVD-R/DVD+R:</p>

<stable-full-dvd-images />
</div><div class="clear"></div>
</div>

<p>Prima di procedere con l'installazione si dovrebbe consultare
la documentazione. <strong>Volendo leggere un solo documento</strong> per
l'installazione, leggere l'<a href="$(HOME)/releases/stable/i386/apa">\
Installation Howto</a>, una <q>passeggiata</q> di tutto il processo di
installazione. Altri documenti utili sono:</p>

<ul>

  <li>La <a href="$(HOME)/releases/stable/installmanual">Guida
  all'installazione</a>, con istruzioni dettagliate per l'installazione</li>

  <li>La <a href="https://wiki.debian.org/DebianInstaller">documentazione del
  Debian-Installer</a>, comprese le FAQ con le domande comuni e relative
  risposte</li>

  <li>L'<a href="$(HOME)/releases/stable/debian-installer/#errata">Errata del
  Debian-Installer</a>, l'elenco dei problemi conosciuti dell'installatore</li>

</ul>

<hr />

# Translators: the following paragraph exists (in this or a similar form) several times in webwml,
# so please try to keep translations consistent. See:
# ./CD/http-ftp/index.wml
# ./CD/live/index.wml
# ./CD/netinst/index.wml
# ./CD/torrent-cd/index.wml
# ./distrib/index.wml
# ./distrib/netinst.wml
# ./releases/<release-codename>/debian-installer/index.wml
# ./devel/debian-installer/index.wml
# 
<div id="firmware_nonfree" class="important">
<p>
Se sul proprio sistema è presente un qualsiasi hardware che
<strong>richiede il caricamento di firmware non-free</strong> insieme ai
driver del dispositivo, è possibile usare uno dei <a
href="https://cdimage.debian.org/cdimage/unofficial/non-free/firmware/bullseye/current/">tarball
con i pacchetti firmware più comuni</a> oppure scaricare
un'immagine <strong>non ufficiale</strong> che contiene i firmware
<strong>non-free</strong>. Le istruzioni su come usare questi tarball e
le informazioni su come caricare il firmware durante l'installazione
possono essere trovate nella <a href="../amd64/ch06s04">guida
all'installazione</a>.
</p>
<p>
<a href="https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/current/">immagini
non ufficiali di <q>stable</q> con firmware incluso</a>
</p>
</div>

<hr />

<h2><a name="mirrors">Mirror conosciuti dell'archivio <q>debian-cd</q></a></h2>

<p>N.B. <strong>alcuni mirror non sono aggiornati</strong>, prima
di scaricare, controllare che il numero di versione delle immagini sia
corrispondente a quello indicato <a href="../#latest">in questo
sito</a>! Inoltre fare attenzione al fatto che non tutti i mirror
contengono l'insieme completo delle immagini (in particolare possono
mancare le immagini dei DVD) a causa della dimensione.</p>

<p><strong>In caso di dubbi usare il
<a href="https://cdimage.debian.org/debian-cd/">server primario delle
immagini dei CD</a> in Svezia</strong> oppure provare il
<a href="https://debian-cd.debian.net/">selettore di mirror automatico
sperimentale</a> che reindirizza automaticamente verso un mirror vicino
e aggiornato.</p>

<p>Per pubblicare dal proprio mirror le immagini dei CD Debian si consultino
le <a href="../mirroring/">istruzioni su come realizzare un mirror per le
immagini dei CD</a>.</p>

#use wml::debian::countries
#include "$(ENGLISHDIR)/CD/http-ftp/cdimage_mirrors.list"
