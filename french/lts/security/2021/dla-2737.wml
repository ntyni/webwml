#use wml::debian::translation-check translation="8820ce05c6cd4a5e4393f3c59325b18dd5ac0629" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans l’environnement
d’exécution Java OpenJDK, aboutissant à un contournement de restrictions
de bac à sable, à une validation incorrecte d’archives JAR signées ou à une
divulgation d'informations.</p>

<p>Merci à Thorsten Glaser et ⮡ tarent pour leur contribution à la mise à jour
des paquets pour corriger ces vulnérabilités.</p>

<p>Pour Debian 9 « Stretch », ces problèmes ont été corrigés dans
la version 8u302-b08-1~deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets openjdk-8.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de openjdk-8,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/openjdk-8">\
https://security-tracker.debian.org/tracker/openjdk-8</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2737.data"
# $Id: $
