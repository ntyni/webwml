#use wml::debian::translation-check translation="0cae5f2cb3aefb7207e9c617ed2d97a18c8258cb" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Une paire de vulnérabilités a été découverte dans src:python3.5, la
version 3.5 de l'interpréteur Python ; ce sont :</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3733">CVE-2021-3733</a>

<p>L'expression rationnelle, vulnérable à un déni de service par expression
rationnelle (ReDoS), a une complexité quadratique dans des cas extrêmes et
elle permet de provoquer un déni de service lors de l'identification de RFC
contrefaits non valables. Ce problème de ReDos est côté client et nécessite
que des attaquants distants contrôlent le serveur HTTP.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3737">CVE-2021-3737</a>

<p>Un client HTTP peut se trouver bloqué dans la lecture infinie de lignes
de longueur <q>len(line) < 64k</q> après la réception d'une réponse HTTP
<q>100 Continue</q>. Cela pourrait conduire à ce que le client devienne un
gouffre de bande passante pour tous ceux qui contrôlent un serveur.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans la
version 3.5.3-1+deb9u5.</p>

<p>Nous vous recommandons de mettre à jour vos paquets python3.5.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de python3.5, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/python3.5">\
https://security-tracker.debian.org/tracker/python3.5</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2808.data"
# $Id: $
