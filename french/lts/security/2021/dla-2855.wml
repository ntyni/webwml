#use wml::debian::translation-check translation="fe862ee11bf34ab52cf5013c337859111ec1bc0d" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Deux vulnérabilités ont été corrigées dans monit, un utilitaire de
surveillance et gestion de systèmes Unix.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11454">CVE-2019-11454</a>

<p>Vulnérabilité de script intersite persistante dans http/cervlet.c</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11455">CVE-2019-11455</a>

<p>Lecture hors limites de tampon dans Util_urlDecode dans util.c</p></li>

</ul>

<p>Pour Debian 9 Stretch, ces problèmes ont été corrigés dans la version
1:5.20.0-6+deb9u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets monit.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de monit veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/monit">\
https://security-tracker.debian.org/tracker/monit</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2855.data"
# $Id: $
