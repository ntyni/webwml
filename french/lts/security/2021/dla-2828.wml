#use wml::debian::translation-check translation="9171db7a63bab4b94ef6ff5c2ac9351e1ed20b9a" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été corrigées dans libvorbis, une
bibliothèque populaire pour le codec audio Vorbis.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14160">CVE-2017-14160</a>
<a href="https://security-tracker.debian.org/tracker/CVE-2018-10393">CVE-2018-10393</a>

<p>Amélioration de la vérification de limites pour des taux
d’échantillonnage très bas.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-10392">CVE-2018-10392</a>

<p>Validation du nombre de canaux dans vorbisenc.c</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans la
version 1.3.5-4+deb9u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libvorbis.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de libvorbis, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/libvorbis">\
https://security-tracker.debian.org/tracker/libvorbis</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2828.data"
# $Id: $
