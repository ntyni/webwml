#use wml::debian::translation-check translation="ec63203ae7c88b70ad002f6a6d89bd22297d4fb8" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans libsnfile, une bibliothèque
pour lire et écrire des fichiers contenant des échantillons sonores.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-8361">CVE-2017-8361</a>

<p>La fonction flac_buffer_copy (flac.c) est sujette à un dépassement de tampon.
Cette vulnérabilité peut être exploitée par des attaquants distants pour
provoquer un déni de service, ou éventuellement avoir un impact non précisé
à l'aide d'un fichier audio contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-8362">CVE-2017-8362</a>

<p>La fonction flac_buffer_copy (flac.c) est sujette à une vulnérabilité de
lecture hors limites. Ce défaut peut être exploité par des attaquants distants
pour provoquer un déni de service à l'aide d'un fichier audio contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-8363">CVE-2017-8363</a>

<p>La fonction flac_buffer_copy (flac.c) est sujette à une vulnérabilité de
lecture hors limites basée sur le tas. Ce défaut peut être exploité par des
attaquants distants pour provoquer un déni de service à l'aide d'un fichier
audio contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-8365">CVE-2017-8365</a>

<p>La fonction i2les_array (pcm.c) est sujette à un dépassement de tampon
global. Cette vulnérabilité peut être exploitée par des attaquants distants pour
provoquer un déni de service, ou éventuellement avoir un impact non précisé
à l'aide d'un fichier audio contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14245">CVE-2017-14245</a> /
<a href="https://security-tracker.debian.org/tracker/CVE-2017-14246">CVE-2017-14246</a> /
<a href="https://security-tracker.debian.org/tracker/CVE-2017-17456">CVE-2017-17456</a> /
<a href="https://security-tracker.debian.org/tracker/CVE-2017-17457">CVE-2017-17457</a>

<p>Les fonctions d2alaw_array() et d2ulaw_array()s (src/ulaw.c et src/alaw.c)
sont sujettes à une vulnérabilité de lecture hors limites. Ce défaut peut être
exploité par des attaquants distants pour provoquer un déni de service ou une
divulgation d'informations à l'aide d'un fichier audio contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14634">CVE-2017-14634</a>

<p>La fonction double64_init() (double64.c) est sujette à une erreur de division
par zéro. Cette vulnérabilité peut être exploitée par des attaquants distants
pour provoquer déni de service à l'aide d'un fichier audio contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-13139">CVE-2018-13139</a>

<p>La fonction psf_memset (common.c) est sujette à un dépassement de pile.
Cette vulnérabilité peut être exploitée par des attaquants distants pour
provoquer un déni de service, ou éventuellement avoir un impact non précisé
à l'aide d'un fichier audio contrefait. La vulnérabilité peut être déclenchée
par un exécutable sndfile-deinterleave.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-19432">CVE-2018-19432</a>

<p>La fonction sf_write_int (src/sndfile.c) est sujette à une vulnérabilité de
lecture hors limites. Ce défaut peut être exploité par des attaquants distants
pour provoquer un déni de service à l'aide d'un fichier audio contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-19661">CVE-2018-19661</a> /
<a href="https://security-tracker.debian.org/tracker/CVE-2018-19662">CVE-2018-19662</a>

<p>Les fonctions i2alaw_array() et i2ulaw_array()s (src/ulaw.c et src/alaw.c)
sont sujettes à une vulnérabilité de lecture hors limites. Ce défaut peut être
exploité par des attaquants distants pour provoquer un déni de service ou une
divulgation d'informations à l'aide d'un fichier audio contrefait.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans la version 1.0.25-9.1+deb8u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libsndfile.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1618.data"
# $Id: $
