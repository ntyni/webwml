#use wml::debian::translation-check translation="f850b90f09d2c777ae909e420b0480dee6fb04ab" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Un problème a été découvert dans Pidgin avant la version 2.14.9. Un
attaquant distant qui peut usurper des réponses DNS peut rediriger une
connexion du client vers un serveur malveillant. Le client réalisera une
vérification du certificat TLS du nom du domaine malveillant à la place de
celui du domaine original du service XMPP, permettant à l'attaquant de
prendre le contrôle sur la connexion XMPP et d'obtenir l'identifiant de
l'utilisateur et tout le contenu de la communication.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans la version
2.12.0-1+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets pidgin.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de pidgin, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/pidgin">\
https://security-tracker.debian.org/tracker/pidgin</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3043.data"
# $Id: $
