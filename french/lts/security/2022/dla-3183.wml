#use wml::debian::translation-check translation="52a5f79a826ef8abc7577beab4f61d5befbce96b" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Les vulnérabilités suivantes ont  été découvertes dans le moteur
web WebKitGTK :</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-42799">CVE-2022-42799</a>

<p>Jihwan Kim et Dohyun Lee ont découvert que la visite d'un site
malveillant pouvait conduire à une usurpation de l'interface utilisateur.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-42823">CVE-2022-42823</a>

<p>Dohyun Lee a découvert que le traitement d'un contenu web contrefait
pouvait conduire à l'exécution de code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-42824">CVE-2022-42824</a>

<p>Abdulrahman Alqabandi, Ryan Shin et Dohyun Lee ont découvert que le
traitement d'un contenu web contrefait pouvait divulguer des informations
sensibles de l'utilisateur.</p></li>

</ul>

<p>Pour Debian 10 Buster, ces problèmes ont été corrigés dans la version
2.38.2-1~deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets webkit2gtk.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de webkit2gtk, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/webkit2gtk">\
https://security-tracker.debian.org/tracker/webkit2gtk</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3183.data"
# $Id: $
