#use wml::debian::translation-check translation="940fc08f633277cc6ff94f3fee14caa4f9fbc736" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Il a été découvert que Log4j2 d'Apache, une infrastructure de
journalisation pour Java, ne protégeait pas contre une récursion non
contrôlée à partir de recherches auto-référencées. Quand la configuration
de la journalisation utilise un schéma de motif autre que celui par défaut
avec une recherche de contexte (par exemple, $${ctx:loginId}), des
attaquants ayant le contrôle sur des données d'entrée de carte de contextes
de thread (MDC) peuvent contrefaire des données d'entrée qui contiennent
une recherche récursive, aboutissant à un déni de service.</p>

<p>Pour la distribution oldstable (Buster), ce problème a été corrigé dans
la version 2.17.0-1~deb10u1.</p>

<p>Pour la distribution stable (Bullseye), ce problème a été corrigé dans
la version 2.17.0-1~deb11u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets apache-log4j2.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de apache-log4j2,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/apache-log4j2">\
https://security-tracker.debian.org/tracker/apache-log4j2</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-5024.data"
# $Id: $
