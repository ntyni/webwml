#use wml::debian::translation-check translation="4d01667976e1539e2c1e4db20e640fadd9b41ed7" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Les vulnérabilités suivantes ont été découvertes dans le moteur web WPE
WebKit :</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-32792">CVE-2022-32792</a>

<p>Manfred Paul a découvert que le traitement d'un contenu web contrefait
pouvait conduire à l'exécution de code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-32816">CVE-2022-32816</a>

<p>Dohyun Lee a découvert que la visite d'un site web qui contient un
<q>frame</q> malveillant pouvait conduire à une usurpation d'interface
utilisateur.</p></li>

</ul>

<p>Pour la distribution stable (Bullseye), ces problèmes ont été corrigés
dans la version 2.36.6-1~deb11u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets wpewebkit.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de wpewebkit, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/wpewebkit">\
https://security-tracker.debian.org/tracker/wpewebkit</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5211.data"
# $Id: $
