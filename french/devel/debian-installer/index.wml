#use wml::debian::template title="Installateur Debian" NOHEADER="true"
#use wml::debian::recent_list
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/devel/debian-installer/images.data"
#use wml::debian::translation-check translation="a22870164df5007ae4f4e356dfe54983be0f1e9e" maintainer="Baptiste Jammet"

<h1>Nouvelles</h1>

<p><:= get_recent_list('News/$(CUR_YEAR)', '2',
'$(ENGLISHDIR)/devel/debian-installer', '', '\d+\w*' ) :>
<a href="News">Nouvelles plus anciennes</a>
</p>

<h1>Installation avec l'installateur Debian</h1>

<p>
<if-stable-release release="bullseye">
<strong>Pour obtenir des informations et les médias d'installation officiels
de Debian <current_release_bullseye></strong>, veuillez consulter la 
<a href="$(HOME)/releases/bullseye/debian-installer">page de Bullseye</a>.
</if-stable-release>
<if-stable-release release="bookworm">
<strong>Pour obtenir des informations et les médias d'installation officiels
de Debian <current_release_bookworm></strong>, veuillez consulter la
<a href="$(HOME)/releases/bookworm/debian-installer">page de Bookworm</a>.
</if-stable-release>
</p>

<div class="tip">
<p>
Toutes les images indiquées ci-dessous embarquent la version de l'installateur
Debian développée pour la prochaine publication Debian et installent par
défaut la version de test de Debian (<q><current_testing_name></q>).
</p>
</div>

<!-- Affiché au début du cycle de publication : pas encore de publication Alpha/Beta/RC. -->
<if-testing-installer released="no">
<p>

<strong>Pour installer la version de test de Debian</strong>, nous vous recommandons
d'utiliser les <strong>images construites quotidiennement</strong> de l'installateur.
Les images suivantes sont disponibles pour les images construites quotidiennement.

</p>

</if-testing-installer>

<!-- Affiché plus tard dans le cycle de publication : Alpha/Beta/RC disponible, pointe vers la dernière. -->
<if-testing-installer released="yes">
<p>

<strong>Pour installer la version de test de Debian</strong>, nous vous recommandons
d'utiliser la version <strong><humanversion /></strong> de
l'installateur après avoir vérifié les <a href="errata">errata</a>. Les images
suivantes sont disponibles pour <humanversion />.

</p>

<h2>Version officielle</h2>

<div class="line">
<div class="item col50">
<strong>images de CD d'installation par le réseau</strong>
<netinst-images />
</div>

<div class="item col50 lastcol">
<strong>images de CD d'installation par le réseau (avec <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong>
<netinst-images-jigdo />
</div>

</div>

<div class="line">
<div class="item col50">
<strong>CD</strong>
<full-cd-images />
</div>

<div class="item col50 lastcol">
<strong>DVD</strong>
<full-dvd-images />
</div>

</div>


<div class="line">
<div class="item col50">
<strong>CD (avec <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong>
<full-cd-jigdo />
</div>

<div class="item col50 lastcol">
<strong>DVD (avec <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong>
<full-dvd-jigdo />
</div>

</div>

<div class="line">
<div class="item col50">
<strong>Blu-ray (avec <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong>
<full-bd-jigdo />
</div>

<div class="item col50 lastcol">
<strong>autres images (amorçage par le réseau, sur clé USB, etc.)</strong>
<other-images />
</div>
</div>

<p>
Ou sinon vous pouvez aussi utiliser un <b>instantané</b> (« snapshot ») de la distribution 
de test de Debian. Les constructions hebdomadaires génèrent un jeu complet d'images
tandis que les constructions quotidiennes ne génèrent que quelques images.
</p>

<div class="warning">

<p>
Ces instantanés installent la distribution de test de Debian, mais l'installateur
est basé sur Debian unstable.
</p>

</div>


<h2>Images hebdomadaires actuelles</h2>

<div class="line">
<div class="item col50">
<strong>CD</strong>
<devel-full-cd-images />
</div>

<div class="item col50 lastcol">
<strong>DVD</strong>
<devel-full-dvd-images />
</div>
</div>

<div class="line">
<div class="item col50">
<strong>CD (avec <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong>
<devel-full-cd-jigdo />
</div>

<div class="item col50 lastcol">
<strong>DVD (avec <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong>
<devel-full-dvd-jigdo />
</div>
</div>

<div class="line">
<div class="item col50">
<strong>Blu-ray (avec <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong>
<devel-full-bd-jigdo />
</div>
</div>

</if-testing-installer>

<h2>Instantanés créés chaque jour</h2>

<div class="line">
<div class="item col50">
<strong>images de CD d'installation par le réseau</strong>
<devel-small-cd-images />
</div>

<div class="item col50 lastcol">
<strong>images de CD d'installation par le réseau (avec <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong>
<devel-small-cd-jigdo/>
</div>
</div>

<div class="line">
<div class="item col50">
<strong>autres images (amorçage par le réseau, clef USB, etc.)</strong>
<devel-other-images />
</div>
</div>

<hr />

<p>
<strong>Notes</strong>
</p>
<ul>
# <li>Avant de télécharger les images construites quotidiennement, nous vous suggérons de
# regarder les <a href="https://wiki.debian.org/DebianInstaller/Today">problèmes connus</a>.</li>
  <li>une architecture peut être (temporairement) omise de la revue des images quotidiennes 
  si les reconstructions quotidiennes ne sont pas disponibles (de façon fiable) ;</li>
  <li>pour les images d'installation, les fichiers de vérification
  (<tt>SHA512SUMS</tt> et <tt>SHA256SUMS</tt>) sont disponibles dans le
  même dossier que les images ;</li>
  <li>pour le téléchargement des images de CD et DVD, l'utilisation de jigdo
  est recommandée&nbsp;;</li>
  <li>seul un nombre limité d'images de jeux de DVD sont disponibles sous forme de
  fichiers ISO disponibles en téléchargement direct ; la plupart des utilisateurs
  n’ont pas besoin de tous les logiciels disponibles sur tous les disques, donc,
  pour préserver la place occupée sur les serveurs de téléchargement et les
  miroirs, les jeux complets ne sont disponibles qu’avec jigdo.</li>
</ul>

<p>
<strong>Après avoir utilisé l'installateur Debian</strong> veuillez nous envoyer
un <a href="https://d-i.debian.org/manual/fr.amd64/ch05s04.html#submit-bug">\
compte-rendu d'installation</a> (en anglais), même si vous n'avez rencontré
aucun problème.</p>

<h1>Documentation</h1>

<p>
<strong>Si vous ne lisez qu'un seul document</strong> avant l'installation, veuillez
lire le <a href="https://d-i.debian.org/manual/fr.amd64/apa.html">guide
d'installation</a>, une présentation rapide du processus d'installation. Voici
d'autres documents utiles&nbsp;:
</p>

<ul>
<li>manuel de l'installation :
#    <a href="$(HOME)/releases/stable/installmanual">version pour la publication actuelle</a>
#    &mdash;
    <a href="$(HOME)/releases/testing/installmanual">version en développement (testing)</a>
    &mdash;
    <a href="https://d-i.debian.org/manual/">dernière version (Git)</a>,
<br />
instructions d'installation détaillées&nbsp;;</li>
<li><a href="https://wiki.debian.org/DebianInstaller/FAQ">FAQ de l'installateur
Debian</a> et <a href="$(HOME)/CD/faq/">FAQ des CD Debian</a>,<br />
questions récurrentes et leurs réponses&nbsp;;</li>
<li><a href="https://wiki.debian.org/DebianInstaller">wiki de l'installateur Debian</a>,<br />
documentation maintenue par la communauté.</li>
</ul>

<h1>Nous contacter</h1>

<p>
La <a href="https://lists.debian.org/debian-boot/">liste de diffusion
debian-boot</a> est le lieu principal des discussions et de travail sur
l'installateur Debian.
</p>

<p>
Nous avons également un canal IRC, #debian-boot sur <tt>irc.debian.org</tt>.
Ce canal est principalement utilisé pour le développement et très
occasionnellement pour fournir de l'aide. Si vous ne recevez pas de réponse,
veuillez essayer sur la liste de diffusion.
</p>
