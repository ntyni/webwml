#use wml::debian::translation-check translation="156615cc19b61bffcfb93a5ff5e5e300fcbc9492" maintainer="Jean-Pierre Giraud"
<define-tag pagetitle>Publication de la mise à jour de Debian 10.8</define-tag>
<define-tag release_date>2021-02-06</define-tag>
#use wml::debian::news
# $Id:

<define-tag release>10</define-tag>
<define-tag codename>Buster</define-tag>
<define-tag revision>10.8</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>
Le projet Debian a l'honneur d'annoncer la huitième mise à jour de sa distribution stable Debian <release> (nom de code <q><codename></q>).
Tout en réglant quelques problèmes importants, cette mise à jour corrige principalement des problèmes de sécurité de la version stable. Les annonces de sécurité ont déjà été publiées séparément et sont simplement référencées dans ce document.
</p>

<p>
Veuillez noter que cette mise à jour ne constitue pas une nouvelle version de Debian <release> mais seulement une mise à jour de certains des paquets qu'elle contient. Il n'est pas nécessaire de jeter les anciens médias de la version <codename>. Après installation, les paquets peuvent être mis à niveau vers les versions courantes en utilisant un miroir Debian à jour.
</p>

<p>
Ceux qui installent fréquemment les mises à jour à partir de security.debian.org n'auront pas beaucoup de paquets à mettre à jour et la plupart des mises à jour de security.debian.org sont comprises dans cette mise à jour.
</p>

<p>
De nouvelles images d'installation seront prochainement disponibles à leurs emplacements habituels.
</p>

<p>
Mettre à jour une installation vers cette révision peut se faire en faisant pointer le système de gestion de paquets sur l'un des nombreux miroirs HTTP de Debian. Une liste complète des miroirs est disponible à l'adresse :
</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>


<h2>Corrections de bogues divers</h2>

<p>
Cette mise à jour de la version stable apporte quelques corrections importantes aux paquets suivants :
</p>

<table border=0>
<tr><th>Paquet</th>               <th>Raison</th></tr>
<correction atftp "Correction d'un problème de déni de service [CVE-2020-6097]">
<correction base-files "Mise à jour de /etc/debian_version pour cette version">
<correction ca-certificates "Mise à jour du paquet CA de Mozilla vers la version 2.40, <q>AddTrust External Root</q> expiré mis en liste noire">
<correction cacti "Correction d'un problème d'injection SQL [CVE-2020-35701] et d'un problème de script intersite (XSS) stocké">
<correction cairo "Correction de l'utilisation de masque dans image-compositor [CVE-2020-35492]">
<correction choose-mirror "Mise à jour de la liste des miroirs">
<correction cjson "Correction de boucle infinie dans cJSON_Minify">
<correction clevis "Correction de la création d'initramfs ; clevis-dracut : création d'initramfs déclenchée à l'installation">
<correction cyrus-imapd "Correction de la comparaison de versions dans le script cron">
<correction debian-edu-config "Déplacement du code de nettoyage des fichiers keytabs de gosa-modify-host vers un script autonome, réduisant les appels de LDAP à une requête unique">
<correction debian-installer "Utilisation de l'ABI du noyau Linux 4.19.0-14 ; reconstruction avec proposed-updates">
<correction debian-installer-netboot-images "Reconstruction avec proposed-updates">
<correction debian-installer-utils "Prise en charge des partitions sur les périphériques USB UAS">
<correction device-tree-compiler "Correction d'erreur de segmentation dans <q>dtc -I fs /proc/device-tree</q>">
<correction didjvu "Ajout de dépendance de construction manquante à tzdata">
<correction dovecot "Correction de plantage lors de la recherche dans des boîtes aux lettres contenant des messages MIME mal formés">
<correction dpdk "Nouvelle version amont stable">
<correction edk2 "CryptoPkg/BaseCryptLib : correction de déréférencement de pointeur NULL [CVE-2019-14584]">
<correction emacs "Pas de plantage quand les ID utilisateur de OpenPGP n'ont pas d'adresse de courriel">
<correction fcitx "Correction de la prise en charge de la méthode d'entrée des logiciels installés avec Flatpak">
<correction file "Augmentation à 50 de la profondeur par défaut de récursion de nom">
<correction geoclue-2.0 "Vérification du niveau maximal de précision permis même pour les applications système ; clé de l'API de Mozilla configurable et utilisation d'une clé spécifique à Debian par défaut ; correction de l'affichage de l'indicateur d'utilisation">
<correction gnutls28 "Correction d'erreur de la suite de tests provoquée par un certificat expiré">
<correction grub2 "Lors de la mise à niveau de grub-pc de façon non interactive, abandon si grub-install échoue ; vérification explicite de l'existence du périphérique cible avant l'exécution de grub-install ; grub-install : ajout de sauvegarde et restauration ; pas d'appel de grub-install sur une installation neuve de grub-pc">
<correction highlight.js "Correction de pollution de prototype [CVE-2020-26237]">
<correction intel-microcode "Mise à jour de divers microprogrammes">
<correction iproute2 "Correction de bogues dans la sortie JSON ; correction d’une situation de compétition qui provoque un déni de service pour le système lors de l'utilisation de <q>ip netns add</q> au démarrage">
<correction irssi-plugin-xmpp "Pas de déclenchement prématuré de la temporisation de connexion du noyau d'irssi, corrigeant ainsi les connexions STARTTLS">
<correction libdatetime-timezone-perl "Mise à jour vers la nouvelle version de tzdata">
<correction libdbd-csv-perl "Correction d'échec de test avec libdbi-perl 1.642-1+deb10u2">
<correction libdbi-perl "Correction de sécurité [CVE-2014-10402]">
<correction libmaxminddb "Correction de lecture hors limites de tampon de tas [CVE-2020-28241]">
<correction lttng-modules "Correction de construction avec les versions du noyau &gt;= 4.19.0-10">
<correction m2crypto "Correction de la compatibilité avec OpenSSL 1.1.1i et suivants">
<correction mini-buildd "builder.py : appel de sbuild : réglé à <q>--no-arch-all</q> de façon explicite">
<correction net-snmp "snmpd : ajout des paramètres cacheTime et execType à EXTEND-MIB">
<correction node-ini "Chaînes aléatoires non valables interdites comme nom de section [CVE-2020-7788]">
<correction node-y18n "Correction d'un problème de pollution de prototype [CVE-2020-7774]">
<correction nvidia-graphics-drivers "Nouvelle version amont ; correction de déni de service et de divulgation d'informations potentiels [CVE-2021-1056]">
<correction nvidia-graphics-drivers-legacy-390xx "Nouvelle version amont ; correction de déni de service et de divulgation d'informations potentiels [CVE-2021-1056]">
<correction pdns "Corrections de sécurité [CVE-2019-10203 CVE-2020-17482]">
<correction pepperflashplugin-nonfree "Transformation en paquet factice en prenant soin de supprimer le greffon précédemment installé (plus fonctionnel ni pris en charge)">
<correction pngcheck "Correction de dépassement de tampon [CVE-2020-27818]">
<correction postgresql-11 "Nouvelle version amont stable ; corrections de sécurité [CVE-2020-25694 CVE-2020-25695 CVE-2020-25696]">
<correction postsrsd "Vérification que les indicateurs d’estampille temporelle ne sont pas trop longs avant d'essayer de les décoder [CVE-2020-35573]">
<correction python-bottle "Plus d'autorisation de <q>;</q> comme séparateur de chaîne de requête [CVE-2020-28473]">
<correction python-certbot "Utilisation automatique de l'API ACMEv2 pour les renouvellements pour éviter des problèmes avec le retrait de l'API ACMEv1">
<correction qxmpp "Correction d'erreur de segmentation potentielle lors d'une erreur de connexion">
<correction silx "python(3)-silx : ajout de dépendance à python(3)-scipy">
<correction slirp "Correction de dépassements de tampon [CVE-2020-7039 CVE-2020-8608]">
<correction steam "Nouvelle version amont">
<correction systemd "journal : pas d'assertion déclenchée quand NULL est passée à journal_file_close()">
<correction tang "Situation de compétition évitée entre keygen et update">
<correction tzdata "Nouvelle version amont ; mise à jour des données de fuseau horaire incluses">
<correction unzip "Application de corrections supplémentaires pour le CVE-2019-13232">
<correction wireshark "Correction de divers plantages, boucles infinies et fuites de mémoire [CVE-2019-16319 CVE-2019-19553 CVE-2020-11647 CVE-2020-13164 CVE-2020-15466 CVE-2020-25862 CVE-2020-25863 CVE-2020-26418 CVE-2020-26421 CVE-2020-26575 CVE-2020-28030 CVE-2020-7045 CVE-2020-9428 CVE-2020-9430 CVE-2020-9431]">
</table>


<h2>Mises à jour de sécurité</h2>


<p>
Cette révision ajoute les mises à jour de sécurité suivantes à la version
stable. L'équipe de sécurité a déjà publié une annonce pour chacune de ces
mises à jour :
</p>

<table border=0>
<tr><th>Identifiant</th>  <th>Paquet</th></tr>
<dsa 2020 4797 webkit2gtk>
<dsa 2020 4801 brotli>
<dsa 2020 4802 thunderbird>
<dsa 2020 4803 xorg-server>
<dsa 2020 4804 xen>
<dsa 2020 4805 trafficserver>
<dsa 2020 4806 minidlna>
<dsa 2020 4807 openssl>
<dsa 2020 4808 apt>
<dsa 2020 4809 python-apt>
<dsa 2020 4810 lxml>
<dsa 2020 4811 libxstream-java>
<dsa 2020 4812 xen>
<dsa 2020 4813 firefox-esr>
<dsa 2020 4814 xerces-c>
<dsa 2020 4815 thunderbird>
<dsa 2020 4816 mediawiki>
<dsa 2020 4817 php-pear>
<dsa 2020 4818 sympa>
<dsa 2020 4819 kitty>
<dsa 2020 4820 horizon>
<dsa 2020 4821 roundcube>
<dsa 2021 4822 p11-kit>
<dsa 2021 4823 influxdb>
<dsa 2021 4824 chromium>
<dsa 2021 4825 dovecot>
<dsa 2021 4827 firefox-esr>
<dsa 2021 4828 libxstream-java>
<dsa 2021 4829 coturn>
<dsa 2021 4830 flatpak>
<dsa 2021 4831 ruby-redcarpet>
<dsa 2021 4832 chromium>
<dsa 2021 4833 gst-plugins-bad1.0>
<dsa 2021 4834 vlc>
<dsa 2021 4835 tomcat9>
<dsa 2021 4837 salt>
<dsa 2021 4838 mutt>
<dsa 2021 4839 sudo>
<dsa 2021 4840 firefox-esr>
<dsa 2021 4841 slurm-llnl>
<dsa 2021 4843 linux-latest>
<dsa 2021 4843 linux-signed-amd64>
<dsa 2021 4843 linux-signed-arm64>
<dsa 2021 4843 linux-signed-i386>
<dsa 2021 4843 linux>
</table>


<h2>Paquets supprimés</h2>

<p>Les paquets suivants ont été supprimés à cause de circonstances hors de
notre contrôle :</p>

<table border=0>
<tr><th>Paquet</th>               <th>Raison</th></tr>
<correction compactheader "Incompatible avec les versions actuelles de Thunderbird">

</table>

<h2>Installateur Debian</h2>
<p>
L'installateur a été mis à jour pour inclure les correctifs incorporés dans cette version de stable.
</p>

<h2>URL</h2>

<p>
Liste complète des paquets qui ont été modifiés dans cette version :
</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>
Adresse de l'actuelle distribution stable :
</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/stable/">
</div>

<p>
Mises à jour proposées à la distribution stable :
</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/proposed-updates">
</div>

<p>
Informations sur la distribution stable (notes de publication, <i>errata</i>, etc.) :
</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>
Annonces et informations de sécurité :
</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>À propos de Debian</h2>
<p>
Le projet Debian est une association de développeurs de logiciels libres qui offrent volontairement leur temps et leurs efforts pour produire le système d'exploitation complètement libre Debian.
</p>

<h2>Contacts</h2>

<p>
Pour de plus amples informations, veuillez consulter le site Internet de Debian <a href="$(HOME)/">https://www.debian.org/</a> ou envoyez un courrier électronique à &lt;press@debian.org&gt; ou contactez l'équipe de publication de la version stable à &lt;debian-release@lists.debian.org&gt;.
</p>
