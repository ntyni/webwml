<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were discovered in salt, a powerful remote
execution manager. The flaws could result in authentication bypass and
invocation of Salt SSH, creation of certificates with weak file
permissions via the TLS execution module or shell injections with the
Salt API using the SSH client.</p>

<p>For the stable distribution (buster), these problems have been fixed in
version 2018.3.4+dfsg1-6+deb10u2.</p>

<p>We recommend that you upgrade your salt packages.</p>

<p>For the detailed security status of salt please refer to its security
tracker page at:
<a href="https://security-tracker.debian.org/tracker/salt">https://security-tracker.debian.org/tracker/salt</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-4837.data"
# $Id: $
