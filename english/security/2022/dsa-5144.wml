<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Several flaws have been discovered in HTCondor, a distributed workload
management system, which allow users with only READ access to any daemon to use
a different authentication method than the administrator has specified. If the
administrator has configured the READ or WRITE methods to include CLAIMTOBE,
then it is possible to impersonate another user and submit or remove jobs.</p>

<p>For the oldstable distribution (buster), these problems have been fixed
in version 8.6.8~dfsg.1-2+deb10u1.</p>

<p>We recommend that you upgrade your condor packages.</p>

<p>For the detailed security status of condor please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/condor">\
https://security-tracker.debian.org/tracker/condor</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5144.data"
# $Id: $
