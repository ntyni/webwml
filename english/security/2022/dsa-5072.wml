<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Marcel Neumann, Robert Altschaffel, Loris Guba and Dustin Hermann
discovered that debian-edu-config, a set of configuration files used for
the Debian Edu blend configured insecure permissions for the user web
shares (~/public_html), which could result in privilege escalation.</p>

<p>If PHP functionality is needed for the user web shares, please refer
to /usr/share/doc/debian-edu-config/README.public_html_with_PHP-CGI+suExec.md</p>

<p>For the oldstable distribution (buster), this problem has been fixed
in version 2.10.65+deb10u8.</p>

<p>For the stable distribution (bullseye), this problem has been fixed in
version 2.11.56+deb11u3.</p>

<p>We recommend that you upgrade your debian-edu-config packages.</p>

<p>For the detailed security status of debian-edu-config please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/debian-edu-config">\
https://security-tracker.debian.org/tracker/debian-edu-config</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5072.data"
# $Id: $
