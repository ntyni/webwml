<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>The following vulnerabilities have been discovered in the WebKitGTK
web engine:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-42852">CVE-2022-42852</a>

    <p>hazbinhotel discovered that processing maliciously crafted web
    content may result in the disclosure of process memory.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-42856">CVE-2022-42856</a>

    <p>Clement Lecigne discovered that processing maliciously crafted web
    content may lead to arbitrary code execution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-42867">CVE-2022-42867</a>

    <p>Maddie Stone discovered that processing maliciously crafted web
    content may lead to arbitrary code execution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-46692">CVE-2022-46692</a>

    <p>KirtiKumar Anandrao Ramchandani discovered that processing
    maliciously crafted web content may bypass Same Origin Policy.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-46698">CVE-2022-46698</a>

    <p>Dohyun Lee and Ryan Shin discovered that processing maliciously
    crafted web content may disclose sensitive user information.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-46699">CVE-2022-46699</a>

    <p>Samuel Gross discovered that processing maliciously crafted web
    content may lead to arbitrary code execution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-46700">CVE-2022-46700</a>

    <p>Samuel Gross discovered that processing maliciously crafted web
    content may lead to arbitrary code execution.</p></li>

</ul>

<p>For the stable distribution (bullseye), these problems have been fixed in
version 2.38.3-1~deb11u1.</p>

<p>We recommend that you upgrade your webkit2gtk packages.</p>

<p>For the detailed security status of webkit2gtk please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/webkit2gtk">\
https://security-tracker.debian.org/tracker/webkit2gtk</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5308.data"
# $Id: $
