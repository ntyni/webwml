<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Etienne Champetier discovered that Docker, a Linux container runtime,
created network bridges which by default accept IPv6 router advertisements.
This could allow an attacker with the CAP_NET_RAW capability in a
container to spoof router advertisements, resulting in information
disclosure or denial of service.</p>

<p>For the stable distribution (buster), this problem has been fixed in
version 18.09.1+dfsg1-7.1+deb10u2.</p>

<p>We recommend that you upgrade your docker.io packages.</p>

<p>For the detailed security status of docker.io please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/docker.io">\
https://security-tracker.debian.org/tracker/docker.io</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4716.data"
# $Id: $
