<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was stack-based buffer over-read in memcached,
the in-memory caching server.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15026">CVE-2019-15026</a>

    <p>memcached 1.5.16, when UNIX sockets are used, has a stack-based buffer
    over-read in <tt>conn_to_str</tt> in <tt>memcached.c</tt>.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1.4.21-1.1+deb8u3.</p>

<p>We recommend that you upgrade your memcached packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1913.data"
# $Id: $
