<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was a path traversal vulnerability in the
mercurial distributed revision version control system.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-3902">CVE-2019-3902</a>

    <p>A flaw was found in Mercurial before 4.9. It was possible to use
    symlinks and subrepositories to defeat Mercurial's path-checking logic and
    write files outside a repository.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
3.1.2-2+deb8u7.</p>

<p>We recommend that you upgrade your mercurial packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1764.data"
# $Id: $
