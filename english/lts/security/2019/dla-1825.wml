<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A reply-based decryption oracle was found in kdepim, which provides
the KMail e-mail client.</p>

<p>An attacker in possession of S/MIME or PGP encrypted emails can wrap
them as sub-parts within a crafted multipart email. The encrypted
part(s) can further be hidden using HTML/CSS or ASCII newline
characters. This modified multipart email can be re-sent by the
attacker to the intended receiver. If the receiver replies to this
(benign looking) email, they unknowingly leak the plaintext of the
encrypted message part(s) back to the attacker.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
4:4.14.1-1+deb8u2.</p>

<p>We recommend that you upgrade your kdepim packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1825.data"
# $Id: $
