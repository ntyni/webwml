<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Two more security issues have been corrected in multiple demuxers and
decoders of the libav multimedia library.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-15822">CVE-2018-15822</a>

    <p>The flv_write_packet function in libavformat/flvenc.c in libav did
    not check for an empty audio packet, leading to an assertion failure.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11338">CVE-2019-11338</a>

    <p>libavcodec/hevcdec.c in libav mishandled detection of duplicate first
    slices, which allowed remote attackers to cause a denial of service
    (NULL pointer dereference and out-of-array access) or possibly have
    unspecified other impact via crafted HEVC data.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
6:11.12-1~deb8u7.</p>

<p>We recommend that you upgrade your libav packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1809.data"
# $Id: $
