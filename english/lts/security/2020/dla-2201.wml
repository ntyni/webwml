<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>A Denial of Service (DoS) vulnerability was discovered in the network time
protocol server/client, ntp.</p>

<p>ntp allowed an "off-path" attacker to block unauthenticated
synchronisation via a server mode packet with a spoofed source IP
address because transmissions were rescheduled even if a packet
lacked a valid "origin timestamp"</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11868">CVE-2020-11868</a>

    <p>ntpd in ntp before 4.2.8p14 and 4.3.x before 4.3.100 allows an off-path
    attacker to block unauthenticated synchronization via a server mode packet
    with a spoofed source IP address, because transmissions are rescheduled
    even when a packet lacks a valid origin timestamp</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1:4.2.6.p5+dfsg-7+deb8u3.</p>

<p>We recommend that you upgrade your ntp packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2201.data"
# $Id: $
