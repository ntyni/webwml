<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that OpenDMARC, a milter implementation of DMARC,
has improper null termination in the function opendmarc_xml_parse that
can result in a one-byte heap overflow in opendmarc_xml when parsing a
specially crafted DMARC aggregate report. This can cause remote memory
corruption when a '\0' byte overwrites the heap metadata of the next
chunk and its PREV_INUSE flag.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
1.3.2-2+deb9u3.</p>

<p>We recommend that you upgrade your opendmarc packages.</p>

<p>For the detailed security status of opendmarc please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/opendmarc">https://security-tracker.debian.org/tracker/opendmarc</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2639.data"
# $Id: $
