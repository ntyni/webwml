<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Apache Tomcat did not properly validate incoming TLS packets. When Tomcat
was configured to use NIO+OpenSSL or NIO2+OpenSSL for TLS, a specially
crafted packet could be used to trigger an infinite loop resulting in a
denial of service.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
8.5.54-0+deb9u8.</p>

<p>We recommend that you upgrade your tomcat8 packages.</p>

<p>For the detailed security status of tomcat8 please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/tomcat8">https://security-tracker.debian.org/tracker/tomcat8</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2764.data"
# $Id: $
