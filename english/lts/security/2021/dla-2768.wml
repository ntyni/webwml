<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that the uwsgi proxy module for Apache2
(mod_proxy_uwsgi) can read above the allocated memory when processing
a request with a carefully crafted uri-path. An attacker may cause the
server to crash (DoS).</p>

<p>For Debian 9 stretch, this problem has been fixed in version
2.0.14+20161117-3+deb9u4.</p>

<p>We recommend that you upgrade your uwsgi packages.</p>

<p>For the detailed security status of uwsgi please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/uwsgi">https://security-tracker.debian.org/tracker/uwsgi</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2768.data"
# $Id: $
