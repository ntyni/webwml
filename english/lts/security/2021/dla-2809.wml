<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Stefan Walter found that udisks2, a service to access and manipulate
storage devices, could cause denial of service via system crash if a
corrupted or specially crafted ext2/3/4 device or image was mounted,
which could happen automatically on certain environments.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
2.1.8-1+deb9u1.</p>

<p>We recommend that you upgrade your udisks2 packages.</p>

<p>For the detailed security status of udisks2 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/udisks2">https://security-tracker.debian.org/tracker/udisks2</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2809.data"
# $Id: $
