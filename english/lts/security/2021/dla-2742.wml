<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple issues have been discovered in ffmpeg.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-21041">CVE-2020-21041</a>

    <p>Buffer Overflow vulnerability exists via apng_do_inverse_blend in
    libavcodec/pngenc.c, which could let a remote malicious user cause a
    Denial of Service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-22015">CVE-2020-22015</a>

    <p>Buffer Overflow vulnerability in mov_write_video_tag due to the out of
    bounds in libavformat/movenc.c, which could let a remote malicious user
    obtain sensitive information, cause a Denial of Service, or execute
    arbitrary code.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-22016">CVE-2020-22016</a>

    <p>A heap-based Buffer Overflow vulnerability at libavcodec/get_bits.h when
    writing .mov files, which might lead to memory corruption and other
    potential consequences.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-22020">CVE-2020-22020</a>

    <p>Buffer Overflow vulnerability in the build_diff_map function in
    libavfilter/vf_fieldmatch.c, which could let a remote malicious user cause
    a Denial of Service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-22021">CVE-2020-22021</a>

    <p>Buffer Overflow vulnerability at filter_edges function in
    libavfilter/vf_yadif.c, which could let a remote malicious user cause a
    Denial of Service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-22022">CVE-2020-22022</a>

    <p>A heap-based Buffer Overflow vulnerability exists in filter_frame at
    libavfilter/vf_fieldorder.c, which might lead to memory corruption and other
    potential consequences.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-22023">CVE-2020-22023</a>

    <p>A heap-based Buffer Overflow vulnerabililty exists in filter_frame at
    libavfilter/vf_bitplanenoise.c, which might lead to memory corruption and
    other potential consequences.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-22025">CVE-2020-22025</a>

    <p>A heap-based Buffer Overflow vulnerability exists in gaussian_blur at
    libavfilter/vf_edgedetect.c, which might lead to memory corruption and other
    potential consequences.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-22026">CVE-2020-22026</a>

    <p>Buffer Overflow vulnerability exists in the config_input function at
    libavfilter/af_tremolo.c, which could let a remote malicious user cause a
    Denial of Service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-22028">CVE-2020-22028</a>

    <p>Buffer Overflow vulnerability in filter_vertically_8 at
    libavfilter/vf_avgblur.c, which could cause a remote Denial of Service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-22031">CVE-2020-22031</a>

    <p>A Heap-based Buffer Overflow vulnerability in filter16_complex_low, which
    might lead to memory corruption and other potential consequences.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-22032">CVE-2020-22032</a>

    <p>A heap-based Buffer Overflow vulnerability in gaussian_blur, which might
    lead to memory corruption and other potential consequences.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-22036">CVE-2020-22036</a>

    <p>A heap-based Buffer Overflow vulnerability in filter_intra at
    libavfilter/vf_bwdif.c, which might lead to memory corruption and other
    potential consequences.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3566">CVE-2021-3566</a>

    <p>The tty demuxer did not have a <q>read_probe</q> function assigned to it. By
    crafting a legitimate <q>ffconcat</q> file that references an image, followed by
    a file the triggers the tty demuxer, the contents of the second file will be
    copied into the output file verbatim (as long as the `-vcodec copy` option
    is passed to ffmpeg).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-38114">CVE-2021-38114</a>

    <p>libavcodec/dnxhddec.c does not check the return value of the  init_vlc
    function. Crafted DNxHD data can cause unspecified impact.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
7:3.2.15-0+deb9u3.</p>

<p>We recommend that you upgrade your ffmpeg packages.</p>

<p>For the detailed security status of ffmpeg please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/ffmpeg">https://security-tracker.debian.org/tracker/ffmpeg</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2742.data"
# $Id: $
