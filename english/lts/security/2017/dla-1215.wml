<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Francesco Sirocco discovered a flaw in otrs2, the Open Ticket Request
System, which could result in session information disclosure when cookie
support is disabled. A remote attacker can take advantage of this flaw
to take over an agent's session if the agent is tricked into clicking a
link in a specially crafted mail.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
3.3.18-1~deb7u3.</p>

<p>We recommend that you upgrade your otrs2 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1215.data"
# $Id: $
