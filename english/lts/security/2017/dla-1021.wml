<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that Jetty8, a Java servlet engine and webserver, was
vulnerable to a timing attack which might reveal cryptographic
credentials such as passwords to a local user.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
8.1.3-4+deb7u1.</p>

<p>We recommend that you upgrade your jetty8 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1021.data"
# $Id: $
