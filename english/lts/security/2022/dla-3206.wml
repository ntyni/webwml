<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple security vulnerabilities were discovered in heimdal, an
implementation of the Kerberos 5 authentication protocol, which may
result in denial of service, information disclosure, or remote code
execution.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-14870">CVE-2019-14870</a>

    <p>Isaac Boukris reported that the Heimdal KDC before 7.7.1 does not
    apply delegation_not_allowed (aka not-delegated) user attributes for
    S4U2Self.  Instead the forwardable flag is set even if the
    impersonated client has the not-delegated flag set.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3671">CVE-2021-3671</a>

    <p>Joseph Sutton discovered that the Heimdal KDC before 7.7.1 does not
    check for missing missing sname in TGS-REQ (Ticket Granting Server     Request) before before dereferencing.  An authenticated user could
    use this flaw to crash the KDC.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-44758">CVE-2021-44758</a>

    <p>It was discovered that Heimdal is prone to a NULL dereference in
    acceptors when the initial SPNEGO token has no acceptable
    mechanisms, which may result in denial of service for a server
    application that uses the Simple and Protected GSSAPI Negotiation
    Mechanism (SPNEGO).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-3437">CVE-2022-3437</a>

    <p>Evgeny Legerov reported that the DES and Triple-DES decryption
    routines in the Heimdal GSSAPI library before 7.7.1 were prone to
    buffer overflow on malloc() allocated memory when presented with a
    maliciously small packet.  In addition, the Triple-DES and RC4
    (arcfour) decryption routine were prone to non-constant time leaks,
    which could potentially yield to a leak of secret key material when
    using these ciphers.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-41916">CVE-2022-41916</a>

    <p>It was discovered that Heimdal's PKI certificate validation library
    before 7.7.1 can under some circumstances perform an out-of-bounds
    memory access when normalizing Unicode, which may result in denial
    of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-42898">CVE-2022-42898</a>

    <p>Greg Hudson discovered an integer multiplication overflow in the
    Privilege Attribute Certificate (PAC) parsing routine, which may
    result in denial of service for Heimdal KDCs and possibly Heimdal
    servers (e.g., via GSS-API) on 32-bit systems.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-44640">CVE-2022-44640</a>

    <p>Douglas Bagnall and the Heimdal maintainers independently discovered
    that Heimdal's ASN.1 compiler before 7.7.1 generates code that
    allows specially crafted DER encodings of CHOICEs to invoke the
    wrong free() function on the decoded structure upon decode error,
    which may result in remote code execution in the Heimdal KDC and
    possibly the Kerberos client, the X.509 library, and other
    components as well.</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
7.5.0+dfsg-3+deb10u1.</p>

<p>We recommend that you upgrade your heimdal packages.</p>

<p>For the detailed security status of heimdal please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/heimdal">https://security-tracker.debian.org/tracker/heimdal</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3206.data"
# $Id: $
