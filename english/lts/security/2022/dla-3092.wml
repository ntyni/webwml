<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A buffer overflow was discovered in the vhost code of DPDK,
a set of libraries for fast packet processing, which could result
in denial of service or the execution of arbitrary code by malicious
guests/containers.</p>

<p>For Debian 10 buster, this problem has been fixed in version
18.11.11-1~deb10u2.</p>

<p>We recommend that you upgrade your dpdk packages.</p>

<p>For the detailed security status of dpdk please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/dpdk">https://security-tracker.debian.org/tracker/dpdk</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3092.data"
# $Id: $
