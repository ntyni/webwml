<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple security vulnerabilities have been found in OpenEXR, command-line
tools and a library for the OpenEXR image format. Buffer overflows or
out-of-bound reads could lead to a denial of service (application crash) if
a malformed image file is processed.</p>

<p>For Debian 10 buster, these problems have been fixed in version
2.2.1-4.1+deb10u2.</p>

<p>We recommend that you upgrade your openexr packages.</p>

<p>For the detailed security status of openexr please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/openexr">https://security-tracker.debian.org/tracker/openexr</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3236.data"
# $Id: $
