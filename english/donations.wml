#use wml::debian::template title="How to donate to the Debian Project" MAINPAGE="true"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<ul class="toc">
  <li><a href="#spi">Software in the Public Interest</a></li>
  <li><a href="#debianfrance">Debian France</a></li>
  <li><a href="#debianch">debian.ch</a></li>
  <li><a href="#debian">Debian</a></li>
</ul>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Thanks to all donors for supporting Debian with equipment or services: <a href="https://db.debian.org/machines.cgi">hosting and hardware sponsors</a>, <a href="mirror/sponsors">mirror sponsors</a>, <a href="partners/">development and service partners</a>.<p>
</aside>

<p>
Donations are managed by the <a href="$(HOME)/devel/leader">Debian Project Leader</a> (DPL) and allow Debian to have <a href="https://db.debian.org/machines.cgi">machines</a>, <a href="https://wiki.debian.org/Teams/DSA/non-DSA-HW">other hardware</a>, domains, cryptographic certificates, <a href="https://www.debconf.org">the Debian conference</a>, <a href="https://wiki.debian.org/MiniDebConf">Debian mini-conferences</a>, <a href="https://wiki.debian.org/Sprints">development sprints</a>, presence at other events, etc.
</p>

<p id="default">
Various <a href="https://wiki.debian.org/Teams/Treasurer/Organizations">organizations</a> hold assets in trust for Debian and receive donations on Debian's behalf. This page lists different methods of donating to the Debian project. The easiest method of donating to Debian is via PayPal to <a href="https://www.spi-inc.org/" title="SPI">Software in the Public Interest</a>, a non-profit organization that holds assets in trust for Debian.
</p>

<p style="text-align:center"><button type="button"><span class="fab fa-paypal fa-2x"></span> <a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&amp;hosted_button_id=86AHSZNRR29UU">Donate via PayPal</a></button></p>

<aside class="light">
  <span class="fas fa-gifts fa-5x"></span>
</aside>

<table>
<tr>
<th>Organization</th>
<th>Methods</th>
<th>Notes</th>
</tr>
<tr>
<td><a href="#spi"><acronym title="Software in the Public Interest">SPI</acronym></a></td>
<td>
 <a href="#spi-paypal">PayPal</a>,
 <a href="#spi-click-n-pledge">Click &amp; Pledge</a>,
 <a href="#spi-cheque">Cheque</a>,
 <a href="#spi-other">Other</a>
</td>
<td>USA, tax-exempt non-profit</td>
</tr>
<tr>
<td><a href="#debianfrance">Debian France</a></td>
<td>
 <a href="#debianfrance-bank">Wire Transfer</a>,
 <a href="#debianfrance-paypal">PayPal</a>
</td>
<td>France, tax-exempt non-profit</td>
</tr>
<tr>
<td><a href="#debianch">debian.ch</a></td>
<td>
 <a href="#debianch-bank">Wire Transfer</a>,
 <a href="#debianch-other">Other</a>
</td>
<td>Switzerland, non-profit</td>
</tr>
<tr>
<td><a href="#debian">Debian</a></td>
<td>
 <a href="#debian-equipment">Equipment</a>,
 <a href="#debian-time">Time</a>,
 <a href="#debian-other">Other</a>
</td>
<td></td>
</tr>

# Template:
#<tr>
#<td><a href="#"><acronym title=""></acronym></a></td>
#<td>
# <a href="#"></a>,
# <a href="#"></a> (allows recurring donations),
# <a href="#cheque"></a> (CUR)
#</td>
#<td>, tax-exempt non-profit</td>
#</tr>

</table>

<h2 id="spi">Software in the Public Interest</h2>

<p>
<a href="https://www.spi-inc.org/" title="SPI">Software in the Public Interest, Inc.</a>
is a tax-exempt non-profit corporation based in the United States of America,
founded by Debian people in 1997 to help free software/hardware organizations.
</p>

<h3 id="spi-paypal">PayPal</h3>

<p>
Single and recurring donations can be made via <a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&amp;hosted_button_id=86AHSZNRR29UU">the SPI page</a> on the PayPal website. To make a recurring donation, check the <em>Make this a monthly donation</em> box.
</p>

<h3 id="spi-click-n-pledge">Click &amp; Pledge</h3>

<p>
Single and recurring donations can be made via <a href="https://co.clickandpledge.com/advanced/default.aspx?wid=34115">the SPI page</a> on the Click &amp; Pledge website.  To make a recurring donation, choose how often you would like to donate on the right (<em>Repeat payment</em>), scroll down to <em>Debian Project Donation</em>, enter the amount you would like to donate, click on the <em>Add to cart</em> item, and go through the rest of the process.
</p>

<h3 id="spi-cheque">Cheque</h3>

<p>
Donations can be made via cheque or money order in <abbr title="United States dollar">USD</abbr> and <abbr title="Canadian dollar">CAD</abbr>.  Please put Debian in the memo field and send the cheque to SPI at the address listed on the <a href="https://www.spi-inc.org/donations/">SPI donations page</a>.
</p>

<h3 id="spi-other">Other</h3>

<p>
Donations via wire transfer and other methods are also available.  For some parts of the world it may be easier to make the donation to one of the partner organizations of Software in the Public Interest.  For more details, please visit the <a href="https://www.spi-inc.org/donations/">SPI donations page</a>.
</p>

<h2 id="debianfrance">Debian France</h2>

<p>
The <a href="https://france.debian.net/">Debian France Association</a> is an organization registered in France under the <q>law of 1901</q>, founded to support and promote the Debian Project in France.
</p>

<h3 id="debianfrance-bank">Wire Transfer</h3>

<p>
Donations via wire transfer are available to the bank account listed on the <a href="https://france.debian.net/soutenir/#compte">Debian France donations page</a>. Receipts are available on request; please send an email to <a href="mailto:donation@france.debian.net">donation@france.debian.net</a>.
</p>

<h3 id="debianfrance-paypal">PayPal</h3>

<p>
Donations can be sent via the <a href="https://france.debian.net/galette/plugins/paypal/form">Debian France PayPal page</a>.  These can be directed to Debian France specifically or the Debian Project in general.
</p>

<h2 id="debianch">debian.ch</h2>

<p>
<a href="https://debian.ch/">debian.ch</a> was founded to represent the Debian project in Switzerland and in the Principality of Liechtenstein.
</p>

<h3 id="debianch-bank">Wire Transfer</h3>

<p>
Donations via wire transfer from both Swiss and international banks are available to the bank accounts listed on the <a href="https://debian.ch/">debian.ch website</a>.
</p>

<h3 id="debianch-other">Other</h3>

<p>
Donations via other methods can be achieved by contacting the donations address listed on the <a href="https://debian.ch/">website</a>.
</p>

# Template:
#<h3 id=""></h3>
#
#<p>
#</p>
#
#<h4 id=""></h4>
#
#<p>
#</p>

<h2 id="debian">Debian</h2>

<p>
Debian is able to accept donations of <a href="#debian-equipment">equipment</a>, but no <a href="#debian-other">other</a> donations at this time.
</p>

<h3 id="debian-equipment">Equipment and Services</h3>

<p>
Debian also relies on the donation of equipment and services from individuals, companies, universities, etc. to keep Debian connected to the world.
</p>

<p>
If your company has any idle machines or spare equipment (hard drives, SCSI controllers, network cards, etc.) lying around, please consider donating them to Debian. Please contact our <a href="mailto:hardware-donations@debian.org">hardware donations delegate</a> for details.
</p>

<p>
Debian maintains a <a href="https://wiki.debian.org/Hardware/Wanted">list of hardware that is wanted</a> for various services and groups within the project.
</p>

<h3 id="debian-time">Time</h3>

<p>
There are many ways to <a href="$(HOME)/intro/help">help Debian</a> during your free time or working time.
</p>

<h3 id="debian-other">Other</h3>

<p>
Debian is not able to accept any cryptocurrencies at this time, but we are looking into being able to support this method of donation.
# Brian Gupta requested we discuss this before including it:
#If you have cryptocurrency to donate, or insights to share, please
#get in touch with <a href="mailto:madduck@debian.org">Martin f. krafft</a>.
</p>
