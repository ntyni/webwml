#use wml::debian::template title="Debian 安全 FAQ"
#include "$(ENGLISHDIR)/security/faq.inc"
#use wml::debian::translation-check translation="1fe91e1ee0d4350b235e726f3f93da47d0639b19"

<maketoc>

<toc-add-entry name=buthow>我通过 debian-security-announce 收到了 DSA，我该如何更新存在漏洞的软件包？</toc-add-entry>

<p>答：正如 DSA 邮件所述，您应该更新受公告漏洞影响的软件包。您可以通过使用 \
  <tt>apt-get upgrade</tt> 更新在您的系统上的所有软件包或使用 \
  <tt>apt-get install <i>package</i></tt> 更新单个特定软件包来做到这一点。</p>

<p>公告邮件中提到了存在漏洞的源软件包。因此，您应该从该源软件包更新所有二进制包。\
   要检查二进制包的更新，请访问 <tt>https://packages.debian.org/src:<i>source-package-name</i></tt> \
   并点击 <i>[show ... binary packages]</i> 中对应的您要更新的发行版。</p>

<p>您也可能需要重新启动服务或正在运行的进程。在软件包 <a \
   href="https://packages.debian.org/debian-goodies">debian-goodies</a> 中包含的 \
   <a href="https://manpages.debian.org/checkrestart"><tt>checkrestart</tt></a> 命令\
   可能有助于查找对应的进程。</p>


<toc-add-entry name=signature>你的公告上的签名验证错误！</toc-add-entry>
<p>答：这很可能是您的问题。在 <a href="https://lists.debian.org/debian-security-announce/">\
   debian-security-announce</a> 邮件列表中有一个过滤器，它只允许发布带有安全团队中一位\
   成员正确的签名的消息。</p>

<p>您的电脑上的某些电邮软件很可能会稍微更改邮件并导致签名被破坏。请确保您的软件不会进行\
   任何 MIME 编码或解码，或者制表符/空格转换。</p>

<p>已知会产生该问题的软件有 fetchmail（启用了 mimedecode 选项）、formail（仅来自 \
   procmail 3.14）以及 evolution。</p>

<toc-add-entry name="handling">Debian 如何处理安全问题？</toc-add-entry>
<p>答：安全团队收到漏洞通知后，一个或多个成员将对其进行审核，并考虑其对 Debian 稳定发行版\
   的影响（即是否受漏洞影响）。如果认为我们的系统会受漏洞影响，我们将针对该问题制造\
   补丁。如果软件包维护者还没有与安全团队联系，那么安全团队也会与他们联系。最后，测试\
   该补丁并准备新的软件包，然后在所有稳定版的体系架构上将其编译并上传。完成所有这些操作后，\
   将发布安全公告。</p>

<toc-add-entry name=oldversion>为什么你要摆弄那个软件包的旧版本呢？</toc-add-entry>

<p>制作修复安全问题的新软件包时，最重要的准则是进行尽可能少的更改。我们的用户和开发人员\
   都依赖于被制作的发行版的确切行为，因此，我们所进行的任何更改都可能损坏某人的系统。对于\
   库，尤其如此：确保无论更改有多小，你都不会更改应用程序编程接口（API）或应用程序二进制\
   接口（ABI）。</p>

<p>这意味着迁移到新的上游版本不是一个好的解决方案，而是应该向后移植相关的更改。通常，上游\
   维护人员愿意在我们需要时提供帮助，不然的话 Debian 安全团队可能会提供帮助。</p>

<p>在某些情况下，例如当需要修改或重写大量源代码时，不可能向后移植一个安全补丁。如果发生\
   这种情况，可能有必要迁移到新的上游版本，但这必须事先与安全团队协调。</p>

<toc-add-entry name=version>软件包的版本号表明我仍在运行受漏洞影响的版本！</toc-add-entry>
<p>答：我们将安全补丁向后移植到稳定发行版中随附的软件包版本，而不是升级到新版本。我们这样做\
   的原因是要确保版本变更尽可能少，以免由于安全补丁造成更改或意外损坏。您可以通过查看软件包\
   更改日志或将其精确版本号与 Debian 安全公告中指明的版本进行比较，以此来检查您是否正在运行\
   一个软件包的安全版本。</p>

<toc-add-entry name=archismissing>我收到了一份公告，但是看上去缺少一种处理器体系架构的构建。</toc-add-entry>
<p>答：通常，安全团队会发布一个包含 Debian 支持的所有体系架构的被更新的软件包的公告。但是，\
   某些架构的构建比其它架构慢，并且可能会发生大多数架构的构建已准备就绪而有些仍不存在的\
   情况。这些较小的架构仅占我们用户群的一小部分。根据问题的紧急程度，我们可能决定立即发布\
   公告报。缺少的构建将在可用时立即被安装，但不会对此另行通知。当然，我们永远不会发布\
   不存在 i386 或 amd64 构建的公告。</p>

<toc-add-entry name=unstable><tt>unstable</tt> 的安全问题是怎样处理的？</toc-add-entry>
<p>答：unstable 的安全问题主要由软件包维护者处理，而不是由 Debian 安全团队处理。尽管当\
   维护者被发现处于非活跃状态时，安全团队可能会上传高紧急程度的仅处理安全问题的修复，但安全\
   团队始终会优先考虑对 stable 的支持。如果您想要拥有一个安全（和稳定）的服务器，强烈建议\
   您保持在稳定版。</p>
   
<toc-add-entry name=testing><tt>testing</tt> 的安全问题是怎样处理的？</toc-add-entry>
<p>答：testing 的安全性得益于对 unstable 整个项目的安全努力。但是，迁移的延迟至少为两天，\
   并且有时安全修复会被转换脚本阻止。安全团队可帮助迁移这些沿着转换脚本生成时会阻止重要\
   安全更新的上传，但这并不总是可能的，并且可能会出现延迟。尤其是在新的稳定版发布后的\
   几个月中，当许多新版本软件包上传到 unstable，用于 testing 的安全补丁可能会滞后。\
   如果您想要拥有一个安全（和稳定）的服务器，强烈建议您保持在稳定版。</p>

<toc-add-entry name=contrib><tt>contrib</tt> 和 <tt>non-free</tt> 的安全问题是怎样处理的？</toc-add-entry>
<p>答：简短的回答是：不会处理。contrib 和 non-free 不是 Debian 发行版的正式组成部分，\
   也没有发布，因此不受安全团队的支持。某些 non-free 软件包是在没有来源或没有一个允许\
   分发修改后的版本的许可证的情况下分发的。在这些情况下，根本无法进行安全修复。如果可以\
   解决问题，并且软件包维护者或其他人提供了正确的更新软件包，那么安全团队通常随后将\
   对其进行处理并发布公告。</p>

<toc-add-entry name=sidversionisold>公告说 unstable 在 1.2.3-1 版本中已修复，但是 unstable  此时的版本是 1.2.5-1，这是怎么回事？</toc-add-entry>
<p>答：我们尝试列出在 unstable 中修复该问题的第一个版本。有时，维护者在此期间上传了甚至\
   比这更新的版本。将在 unstable 的软件包版本与我们指明的版本进行比较。如果相同或更高，\
   您应该免受此漏洞的影响。如果您想要确保这一点的话，可以使用 \
   <tt>apt-get changelog package-name</tt> 来检查软件包变更日志，并搜索宣告该修复的条目。</p>

<toc-add-entry name=mirror>为什么没有 security.debian.org 的官方镜像？</toc-add-entry>
<p>答：事实上这是存在的。有几个通过 DNS 别名实现的官方镜像。security.debian.org 的宗旨是\
   使安全更新尽可能快且容易地获得。</p>

<p>鼓励使用非官方的镜像会增添通常来说没有必要的额外复杂性，而且如果这些镜像没有及时更新\
   的话会导致各种问题。</p>

<toc-add-entry name=missing>我看到了 DSA 100 和 DSA 102，此时 DSA 101 在哪里呢？</toc-add-entry>
<p>答：几个提供方（主要是 GNU/Linux，但也包括 BSD 衍生品的提供方）协调某些漏洞的安全公告\
   并定下特定的时间表，以便所有提供方都可以同时发布公告。做出此决定是为了不区分一些需要\
   更多时间的提供方（例如，当提供方必须经过冗长的 QA 测试通过软件包或者必须支持多种体系架构\
   或二进制分发时）。我们自己的安全团队也会提前准备公告。时不时的，在放置的公告能被发布\
   之前还必须处理其它安全问题，因此会临时按编号保留一个或多个公告。
</p>

<toc-add-entry name=contact>我怎么才能联系安全团队？</toc-add-entry>

<p>答：可以将安全相关的信息发送到 security@debian.org 或 team@security.debian.org，这两个\
   邮箱的信息都会被安全团队的成员读取。
</p>

<p>如果有需要的话，可以使用 Debian 安全联系密钥（密钥 ID <a
   href="https://keyserver.ubuntu.com/pks/lookup?op=get&search=0x0d59d2b15144766a14d241c66baf400b05c3e651">\
   0x0D59D2B15144766A14D241C66BAF400B05C3E651</a>）对电子邮件进行加密。若想取得各个团队成员\
   的 PGP/GPG 公钥，请查看 <a href="https://keyring.debian.org/">keyring.debian.org</a> \
   密钥服务器。</p>

<toc-add-entry name=discover>我猜我找到了一个安全问题，对此我该怎么办呢？</toc-add-entry>

<p>答：如果您在自己负责的一个软件包或在其他人负责的软件包找到了一个安全问题，请始终与安全\
   团队联系。如果 Debian 安全团队确认该漏洞并且其它提供方也可能会受漏洞影响，那么他们\
   通常也会与其它提供方联系。如果该漏洞尚未公开，他们将尝试与其它提供方协调安全公告，因此\
   所有主要的发行版都会同步公告。</p>

<p>如果该漏洞已经被公开，请确保在 Debian BTS 中提交错误报告，并将其标记为<q>security</q>。</p>

<p>如果您是 Debian 维护者，请<a href="#care">参见下文</a>。</p>

<toc-add-entry name=care>我的软件包出现了一个安全问题，我应该怎么处理？</toc-add-entry>

<p>答：如果您在您的软件包中或其他人的软件包中发现了安全问题，请务必通过电子邮件\
   地址 team@security.debian.org 联系安全团队。他们跟踪现有的安全问题、可以帮助\
   维护者解决安全问题或自行修复问题，并且负责发送安全建议和维护 security.debian.org。</p>

<p><a href="$(DOC)/developers-reference/pkgs.html#bug-security">\
   开发者参考文档</a>列出了详细的处理步骤。</p>

<p>特别重要的是，未经安全团队事先同意，请您不要将修复上传到除 unstable 以外的\
   任何发行版，因为绕过安全团队会导致混乱并增加工作量。</p>

<toc-add-entry name=enofile>我尝试下载某个安全公告中列出的软件包，但出现了<q>找不到文件</q>错误。</toc-add-entry>

<p>答：每当新的缺陷修复取代了 security.debian.org 上的旧软件包时，在上传新\
   软件包时旧软件包被删除的可能性很高。所以，您可能收到<q>找不到文件</q>错误。\
   我们希望尽可能减少分发具有已知安全漏洞的软件包的时长。</p>

<p>请使用最新安全公告中的软件包，它们通过 <a
   href="https://lists.debian.org/debian-security-announce/">\
   debian-security-announce</a> 邮件列表分发。最好是在升级软件包前简单地\
   执行 <code>apt-get update</code>。</p>

<toc-add-entry name=upload>我有一个缺陷修复，我可以直接上传到 security.debian.org 吗？</toc-add-entry>

<p>答：不能。 security.debian.org 仓库由安全团队维护，必须由他们批准所有软件包。\
   您应该通过 team@security.debian.org 将补丁或适当的源代码包发送给安全团队。\
   它们将由安全团队进行审查并最终上传，可能经过或未经修改。</p>

<p><a href="$(DOC)/developers-reference/pkgs.html#bug-security">\
   开发者参考文档</a>列出了详细的处理步骤。</p>

<toc-add-entry name=ppu>我有一个缺陷修复，那我可以上传到 proposed-updates 吗？</toc-add-entry>

<p>答：从技术上讲，您可以。但是，您不应该这样做，因为这会严重干扰安全团队的工作。\
来自 security.debian.org 的软件包会被自动复制到 proposed-updates 目录中。\
如果已将具有相同或更高版本号的软件包上传到仓库中，则仓库系统将拒绝此安全更新。\
这样一来，stable 发行版最终将不会对此包进行安全更新，除非 proposed-updates 目录\
中的<q>错误的</q>包被拒绝。请联系安全团队、提供漏洞的所有详细信息，并在您的\
邮件中添加源码文件（即 diff.gz 和 dsc 文件）作为附件。</p>

<p><a href="$(DOC)/developers-reference/pkgs.html#bug-security">\
   开发者参考文档</a>列出了详细的处理步骤。</p>

<toc-add-entry name=SecurityUploadQueue>我很确定我的软件包没问题，我该如何上传它们？</toc-add-entry>

<p>答：如果您非常确定您的软件包不会让任何东西出问题、版本是正常的（即大于 stable，\
   小于 testing 和 unstable）、您没有改变包的行为（除了修复对应的安全问题）、您已\
   针对正确的发行版进行编译（即 <code>oldstable-security</code> 或 \
   <code>stable-security</code>）、如果包是新上传到 security.debian.org 上的那么\
   它已经包含了源代码、您可以确认您的补丁相对于最新版本是干净的并且只涉及相应的\
   安全问题（检查 <code>interdiff -z</code> 和两个 <code>.diff.gz</code> 文件）、\
   您至少已经对补丁进行了三次校对、<code>debdiff</code> 没有显示任何更改，那么您\
   可以将文件直接上传到 security.debian.org 上的 incoming 目录 \
   <code>ftp://ftp.security.upload.debian.org/pub/SecurityUploadQueue</code>。\
   请同时将包含所有详细信息和链接的通知也发送到 team@security.debian.org。</p>

<toc-add-entry name=help>我应该如何协助安全团队的工作？</toc-add-entry>
<p>答：请在向 security@debian.org 报告之前复查每个问题。如果您能够提供补丁，\
还能加快工作进度。不要简单地转发 bugtraq 邮件，因为我们已经收到了它们——但\
可以向我们提供有关 bugtraq 上报告的问题的额外信息。</p>

<p>开始安全工作的一个不错的方式是帮助 Debian Security Tracker（<a
   href="https://security-tracker.debian.org/tracker/data/report">指引</a>）。</p>

<toc-add-entry name=proposed-updates>proposed-updates 的范围是什么？</toc-add-entry>
<p>答：该目录包含被提议进入下一个 Debian stable 修订版本的软件包。 每当维护者\
   为 stable 发行版上传软件包时，它们会先出现在 proposed-updates 目录中。 由于 \
   stable 需要稳定，因此不会进行自动更新。 安全团队会将其公告中提到的已修复的\
   软件包上传到 stable，但它们将首先被上传到 proposed-updates 中。 每隔几个月，
   稳定版发布管理员会检查 proposed-updates 中的软件包列表，并讨论一个软件包\
   是否适合稳定版。 它会被编译成 stable 的另一个修订版本（例如 2.2r3 或 2.2r4）。\
   不适合的软件包也可能会被拒绝并从 proposed-updates 中删除。</p>

<p>请注意，维护者（不是安全团队）在 proposed-updates/ 目录中上传的软件包不受安全团队支持。</p>

<toc-add-entry name=composing>安全团队由哪些人员构成？</toc-add-entry>
<p>答：Debian 安全团队包括<a href="../intro/organization#security">几名官员和秘书</a>。\
   由安全团队本身来任命人员加入团队。</p>

<toc-add-entry name=lifespan>将会提供多长时间的安全更新？</toc-add-entry>
<p>答：安全团队尝试在下一个 stable 发行版发布后的大约一年内支持 stable 发行版，\
   除非在一年内发布另一个 stable 发行版。不可能支持三个发行版； 同时支持两个已经够难的了。</p>

<toc-add-entry name=check>我应该如何检查软件包的完整性？</toc-add-entry>
<p>答：这一过程涉及到检查 Release 文件的签名是否和仓库使用的<a
   href="https://ftp-master.debian.org/keys.html">公钥</a>匹配。Release 文件包含\
   了 Packages 和 Sources 文件的校验和，而 Packages 和 Sources 文件又包含了二进制包\
   和源码包的校验和。检查软件包完整性的详细步骤可以在 <a
   href="$(HOME)/doc/manuals/securing-debian-howto/ch7#s-deb-pack-sign">\
   Debian 安全手册</a>中找到。</p>

<toc-add-entry name=break>如果在安全更新之后，某个其他的软件包出问题了怎么办？</toc-add-entry>
<p>答：首先，您应该弄清楚软件包出问题的原因以及它为何与安全更新有关。如果问题\
   严重，请联系安全团队；如果不严重，请联系稳定版发布管理员。我们讨论的是某个随机\
   的软件包在另一个软件包进行安全更新以后就无法工作的问题。如果您无法弄清楚出了\
   什么问题但需要修复，请与安全团队联系。不过，可能最后还是会让您找稳定版发布管理员。</p>

<toc-add-entry name=cvewhat>什么是 CVE 标识符？</toc-add-entry>
<p>答：Common Vulnerabilities and Exposures 项目为特定安全漏洞分配唯一名称\
（称为 CVE 标识符），以让独一无二地指代特定问题变得更容易。更多信息参见<a
   href="https://en.wikipedia.org/wiki/Common_Vulnerabilities_and_Exposures">\
   维基百科</a>。</p>

<toc-add-entry name=cvedsa>Debian 对每个 CVE id 都会发布一个 DSA 吗？</toc-add-entry>
<p>答：Debian 安全团队跟踪每个发布的 CVE 标识符，将其关联到相关的 Debian 软件包\
   并评估其在 Debian 环境中的影响——一个问题被分配了 CVE id 并不一定意味着该问题\
   对 Debian 系统是一个严重的威胁。这一信息在 <a
   href="https://security-tracker.debian.org">Debian Security Tracker</a> 中\
   进行跟踪，对于我们认为严重的问题，将发布 Debian 安全公告。</p>

<p>不符合发布 DSA 条件的低影响问题可以在 Debian 的下一个版本、当前 stable 或 \
   oldstable 发行版的小版本中修复，或者被包含在针对更严重的漏洞发布的 DSA 中。</p>

<toc-add-entry name=cveget>Debian 可以分配 CVE 标识符吗？</toc-add-entry>
<p>答：Debian 是一个 CVE 编号机构，可以分配标识符，但根据 CVE 政策，只能分配给\
   尚未公开的问题。如果您在 Debian 中的软件中发现了一个未公开的安全漏洞并希望为它\
   获得一个标识符，请联系 Debian 安全团队。 对于漏洞已经公开的情况，我们建议遵循 <a
   href="https://github.com/RedHatProductSecurity/CVE-HOWTO">CVE
   OpenSource Request HOWTO</a> 中描述的程序。</p>

<toc-add-entry name=disclosure-policy>Debian 有漏洞披露政策吗？</toc-add-entry>
<p>答：作为参与 CVE 项目的一部分，Debian 已经发布了一份<a
   href="disclosure-policy">漏洞披露政策</a>。</p>

<h1>已弃用的 Debian 安全 FAQ</h1>

<toc-add-entry name=localremote><q>local (remote)</q>是什么意思？</toc-add-entry>
<p><b>DSA 邮件中的 <i>Problem type</i> 字段自 2014 年 4 月起已经不再使用。</b><br/>
   答：一些公告涵盖了无法通过<q>是否可以本地/远程利用</q>这一经典方式进行归类的漏洞。\
   某些漏洞无法远程利用，即无法和监听网络端口的守护进程相对应。如果这些漏洞能被可以\
   通过网络提供的特殊文件利用，但易受攻击的服务没有永久连接到网络，我们在这种\
   情况下说漏洞的类型是<q>local (remote)</q>。</p>

<p>此类漏洞在某种程度上介于本地和远程漏洞之间，通常涉及可以通过网络提供的文件，\
   例如从邮件附件或下载页面得到的文件。</p>

