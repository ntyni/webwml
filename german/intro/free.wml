#use wml::debian::template title="Was bedeutet Frei?" MAINPAGE="true"

#use wml::debian::translation-check translation="2de158b0e259c30eefd06baf983e4930de3d7412"
# Translator: Dirk Niemeyer <dirk.niemeyer@num.de>
# Changed-by: Martin Schulze <joey@debian.org>
# Update: Holger Wansing <linux@wansing-online.de>, 2012, 2014, 2015.
# Update: Holger Wansing <hwansing@mailbox.org>, 2022.

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<div id="toc">
  <ul class="toc">
    <li><a href="#freesoftware">Frei wie in ...?</a></li>
    <li><a href="#licenses">Software-Lizenzen</a></li>
    <li><a href="#choose">Wie Sie die richtige Lizenz auswählen</a></li>
  </ul>
</div>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Im Februar 1998 gab es
    eine Bewegung, um den Begriff <q><a href="https://www.gnu.org/philosophy/free-sw">Free
    Software</a></q> durch <q><a href="https://opensource.org/docs/definition.html">\
    Open Source Software</a></q> zu ersetzen.
    Die Debatte über diese Begrifflichkeiten spiegelt die Unterschiede
    in der zugrunde liegenden Philosophie wieder, aber die praktischen
    Anforderungen, die an Software-Lizenzen gestellt werden, sowie die
    restliche Thematik auf dieser Seite sind grundsätzlich für Freie
    Software und Open-Source-Software identisch.
</p>
</aside>


<h2><a id="freesoftware">Frei wie in ...?</a></h2>

<p>Viele Menschen, für die Freie Software neu ist, sind verwirrt von dem Begriff
   <q>frei</q>. Er wird nicht so gebraucht, wie sie es erwarten &ndash;
   für sie hat <q>frei</q> die Bedeutung <q>kostenlos</q>.
   Wenn Sie in ein Englisch-Wörterbuch schauen, finden Sie fast zwanzig
   verschiedene Bedeutungen für <q>free</q>, und nur eine davon ist
   <q>unentgeltlich, kostenlos</q>. Der Rest bezieht sich auf Freiheit
   und das Fehlen von Zwängen. Wenn wir von <em>Freier Software</em> sprechen,
   meinen wir eher deren freie Nutzbarkeit und nicht deren Preis.
</p>

<p>Software, die nur in dem Sinne frei ist, dass Sie nichts für deren Nutzung
   zahlen müssen, ist kaum wirklich frei. Es ist Ihnen vielleicht verboten,
   sie weiterzugeben. Und Sie werden höchstwahrscheinlich daran gehindert, sie
   zu verändern. Software mit einer kostenlosen Lizenz ist häufig eine
   Waffe in einer Werbekampagne, um ein verwandtes Produkt zu fördern oder
   einen kleineren Wettbewerber aus dem Markt zu drängen. Es gibt keine
   Garantie, dass sie frei bleibt.
</p>

<p>Für den Uneingeweihten ist ein Stück Software entweder frei oder nicht frei.
   Das reale Leben ist allerdings viel komplizierter als das. Um zu verstehen,
   was impliziert wird, wenn Software als frei bezeichnet wird, müssen wir einen
   kleinen Ausflug in die Welt der Software-Lizenzen machen.
</p>

<h2><a id="licenses">Software-Lizenzen</a></h2>

<p>Copyrights (Urheberrechtsangaben) sind eine Methode, die Rechte des Erschaffers
   bestimmter Werke zu schützen. In den meisten Ländern ist Software, die man
   schreibt, automatisch urheberrechtlich geschützt. Eine Lizenz ist die Möglichkeit
   des Autors, die Nutzung seines Werkes (Software in diesem Fall) durch andere
   auf eine Art zu erlauben, die für ihn akzeptabel ist. Es ist Sache des Autors,
   eine Lizenz beizufügen, die bestimmt, auf welche Weise die Software genutzt werden kann.
</p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="https://www.copyright.gov/">Lesen Sie mehr über Copyrights</a></button></p>

<p>Natürlich erfordern unterschiedliche Umstände auch unterschiedliche
   Arten von Lizenzen. Software-Firmen sind bestrebt, ihre Aufwendungen zu sichern,
   so dass sie nur kompilierten Code veröffentlichen, der für Menschen nicht lesbar ist.
   Und sie schränken die Nutzungsmöglichkeiten dieser Software stark ein.
   Andererseits suchen Autoren freier Software nach anderen Ansätzen, oft sogar
   nach Kombinationen der folgenden Punkte:
</p>

<ul>
  <li>Nicht zuzulassen, dass ihr Code in proprietärer Software verwendet wird. Da
      sie den Code zur Nutzung durch alle veröffentlichen, möchten sie nicht, dass andere
      ihn stehlen. In diesem Fall wird die Verwendung des Codes als ein Zeichen von Vertrauen
      angesehen: Sie dürfen ihn verwenden, solange Sie nach denselben Regeln spielen.</li>
  <li>Die Identität des Urhebers muss geschützt sein. Menschen sind sehr stolz auf ihr
      Werk und wollen nicht, dass andere ihren Namen daraus entfernen oder behaupten,
      sie hätten die Software selbst geschrieben.</li>
  <li>Der Quellcode muss verbreitet werden. Eines der Probleme mit den meisten proprietären
      Software-Produkten ist, dass man Fehler nicht beheben kann oder sie nicht für eine
      bestimmte Anwendung anpassen kann, da der Quellcode nicht verfügbar ist. Darüber
      hinaus könnte die Firma sich vielleicht entscheiden, diejenige Hardware, die man
      gerade nutzt, nicht mehr zu unterstützen. Viele freie Lizenzen erzwingen die
      Verbreitung des Quellcodes. Dies schützt den Nutzer, da es ihm Anpassungen für
      seine Zwecke erlaubt.</li>
  <li>Alle Produkte, die einen Teil der Arbeit eines Autors enthalten (im Bereich
      Urheberrecht nennt man dies <em>abgeleitete Werke</em>), müssen die gleiche
      Lizenz nutzen.</li>
</ul>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Drei der am weitesten verbreiten 
   Lizenzen sind die
   <a href="https://www.gnu.org/copyleft/gpl.html">GNU General Public License (GPL)</a>
   (hier eine <a href="http://www.gnu.de/documents/gpl-3.0.de.html">inoffizielle
    deutsche Übersetzung</a>),
   die <a href="https://opensource.org/licenses/artistic-license.php">Artistic License</a>
   und die <a href="https://opensource.org/licenses/BSD-3-Clause">BSD Style License</a>.
</aside>

<h2><a id="choose">Wie Sie die richtige Lizenz auswählen</a></h2>

<p>Manchmal schreiben Leute ihre eigene Lizenz. Dies kann problematisch
   werden, daher wird es in der Gemeinschaft eher missbilligt.
   Zu oft ist entweder die Aussage zweideutig oder einzelne Bedingungen
   sind widersprüchlich. Die Formulierung einer Lizenz, die vor Gericht
   Bestand hat, ist sogar noch schwieriger. Zum Glück gibt es bereits einige
   Open-Source-Lizenzen zur Auswahl. Sie haben folgendes gemeinsam:
</p>

<ul>
  <li>Man kann die lizenzierte Software auf beliebig vielen Maschinen
      installieren.</li>
  <li>Beliebig viele Personen können diese Software gleichzeitig benutzen.</li>
  <li>Man kann so viele Kopien dieser Software erzeugen wie man will oder
      benötigt und sie an wen auch immer weiter verteilen (freie oder 
      offene Verbreitung).</li>
  <li>Es gibt keine Beschränkung bei der Modifizierung dieser Software
      (außer, dass bestimmte Anmerkungen erhalten bleiben müssen).</li>
  <li>Es gibt keine Einschränkung bezüglich der Weitergabe
      oder gar des Verkaufs dieser Software.</li>
</ul>

<p>Speziell der letzte Punkt, der den Verkauf von Software gegen Geld erlaubt,
   scheint im Gegensatz zur Idee von freier Software zu stehen. Tatsächlich
   ist dies auch eine ihrer Stärken. Da die Lizenz die freie Weiterverbreitung
   erlaubt, kann jeder, der eine Kopie besitzt, weitere Kopien verbreiten.
   Man kann sogar versuchen, sie zu verkaufen.
</p>

<p>Während freie Software nicht komplett frei von Zwängen ist, gibt sie doch
   dem Anwender die Flexibilität, das zu tun, was zur Erreichung seines Ziels
   nötig ist. Gleichzeitig werden die Rechte des Urhebers geschützt.
   Nun, das ist Freiheit.
   Das Debian-Projekt und seine Mitglieder sind ein starker Unterstützer
   freier Software. Wir haben die
   <a href="../social_contract#guidelines">Debian-Richtlinien für Freie Software
   (DFSG)</a> entwickelt. Sie stellen eine angemessene Definition dessen dar, was
   freie Software nach unserer Meinung ausmacht. Nur Software, die der DFSG
   entspricht, kann Bestandteil des Hauptteils (<q>main distribution</q>) von
   Debian sein.
</p>
