#use wml::debian::ddp title="Debian-Dokumentations-Projekt"
#use wml::debian::translation-check translation="60eea6916091f1a3a9240f472ebea1904c32d0bd"
# $Id$
# Translated by Thomas Strathmann <thomas.strathmann@t-online.de>
# Updated: Holger Wansing <hwansing@mailbox.org>, 2020.


<p>Das Debian-Dokumentations-Projekt wurde gegründet, um die Bemühungen,
mehr und bessere Dokumentation für das Debian-System zu schreiben, zu
koordinieren.</p>

  <h2>DDP-Arbeit</h2>
<div class="line">
  <div class="item col50">

    <h3>Handbücher</h3>
    <ul>
      <li><strong><a href="user-manuals">Benutzer-Handbücher</a></strong></li>
      <li><strong><a href="devel-manuals">Entwickler-Handbücher</a></strong></li>
      <li><strong><a href="misc-manuals">Verschiedene Handbücher</a></strong></li>
      <li><strong><a href="#other">Problematische Handbücher</a></strong></li>
    </ul>

  </div>

  <div class="item col50 lastcol">

    <h3>Documentation Policy</h3>
    <ul>
      <li>Handbuch-Lizenzen stimmen mit den DFSG überein.</li>
      <li>Wir verwenden Docbook XML für unsere Dokumente.</li>
      <li>Die Quelltexte sollten unter <a href="https://salsa.debian.org/ddp-team">https://salsa.debian.org/ddp-team</a> liegen.</li>
      <li><tt>www.debian.org/doc/&lt;handbuch-name&gt;</tt> wird die offizielle URL sein.</li>
      <li>Jedes Dokument sollte aktiv betreut sein.</li>
      <li>Bitte fragen Sie auf <a href="https://lists.debian.org/debian-doc/">debian-doc</a>, wenn Sie ein neues Dokument schreiben möchten.</li>
    </ul>

    <h3>GIT-Zugang</h3>
    <ul>
      <li><a href="vcs">Wie erreicht</a> man die GIT-Depots des DDP</li>
    </ul>

  </div>
</div>

<hr />


<h2><a name="other">Problematische Handbücher</a></h2>

<p>Zusätzlich zu den allgemein beworbenen Handbüchern betreuen wir auch die
folgenden Handbücher, die jedes auf seine Art problematisch sind, und wir Sie
daher nicht für alle Benutzer empfehlen können. Haftung ist
ausgeschlossen.</p>

<ul>
  <li><a href="obsolete#tutorial">Debian Tutorial</a>, veraltet</li>
  <li><a href="obsolete#guide">Debian Guide</a>, veraltet</li>
  <li><a href="obsolete#userref">Debian-Anwender-Referenzhandbuch</a>,
      wird nicht mehr entwickelt und ist ziemlich unvollständig</li>
  <li><a href="obsolete#system">Debian
      System-Administratoren-Handbuch</a>, wird nicht mehr entwickelt, beinahe
      leer</li>
  <li><a href="obsolete#network">Debian
      Netzwerk-Administrator-Handbuch</a>, wird nicht mehr entwickelt,
      unvollständig</li>
  <li><a href="obsolete#swprod">Wie Software-Hersteller ihre Produkte
      direkt im .deb-Format bereitstellen können</a>, wird nicht mehr entwickelt,
      veraltet</li>
  <li><a href="obsolete#packman">Debian Paketierungs-Handbuch</a>,
      teilweise in das <a href="devel-manuals#policy">Debian Policy
      Handbuch</a> eingearbeitet, der Rest wird in ein dpkg-Referenzhandbuch
      eingefügt, an dem im Moment geschrieben wird</li>
  <li><a href="obsolete#makeadeb">Einführung: Erstellung eines
      Debian-Pakets</a>, veraltet durch den
      <a href="devel-manuals#maint-guide">Debian-Leitfaden für Neue
      Paketbetreuer</a></li>
  <li><a href="obsolete#programmers">Debian Programmierer Handbuch</a>,
      veraltet durch den
      <a href="devel-manuals#maint-guide">Debian-Leitfaden für Neue
      Paketbetreuer</a>
      und den
      <a href="devel-manuals#debmake-doc">Leitfaden für Debian-Paketbetreuer</a></li>
  <li><a href="obsolete#repo">Debian-Depot-HOWTO</a>, ist seit der Einführung
      von secure APT veraltet</li>
  <li><a href="obsolete#i18n">Einführung in i18n</a>, eingestellt</li>
  <li><a href="obsolete#sgml-howto">Debian SGML/XML Howto</a>, eingestellt, überholt</li>
  <li><a href="obsolete#markup">Debiandoc-SGML-Markup-Handbuch</a>, eingestellt;
      DebianDoc wird aus dem Archiv entfernt werden</li>

</ul>
