#use wml::debian::template title="Kwetsbaarheden van GRUB2 UEFI SecureBoot - 2021"
#use wml::debian::translation-check translation="eaa2e2477454c72064e63ad9f58a59ec94b9d105"

<p>
Sinds de groep van
<a href="$(HOME)/security/2020-GRUB-UEFI-SecureBoot">"BootHole"</a>-bugs
in GRUB2 die in juli 2020 werden bekendgemaakt, zijn beveiligingsonderzoekers
en ontwikkelaars in Debian en elders blijven zoeken naar nog andere problemen die het mogelijk zouden maken om UEFI Secure Boot (UEFI veilige
opstart) te omzeilen. Er werden er verschillende gevonden. Zie het
<a href="$(HOME)/security/2021/dsa-4867">Debian Beveiligingsadvies
4867-1</a> voor meer volledige informatie. Het doel van dit document is om de gevolgen van deze kwetsbaarheid op het gebied van beveiliging uit te leggen en toe te lichten welke stappen ondernomen werden om deze aan te pakken. </p>

<ul>
  <li><b><a href="#what_is_SB">Achtergrond: wat is UEFI Secure Boot?</a></b></li>
  <li><b><a href="#grub_bugs">Meerdere GRUB2-bugs aangetroffen</a></b></li>
  <li><b><a href="#revocations">Er moeten sleutels ingetrokken worden om de Secure-Boot-keten te repareren</a></b></li>
  <li><b><a href="#revocation_problem">Welke effecten heeft het intrekken van de sleutels?</a></b></li>
  <li><b><a href="#package_updates">Bijgewerkte pakketten en sleutels</a></b>
  <ul>
    <li><b><a href="#grub_updates">1. GRUB2</a></b></li>
    <li><b><a href="#linux_updates">2. Linux</a></b></li>
    <li><b><a href="#shim_updates">3. Shim en SBAT</a></b></li>
    <li><b><a href="#fwupdate_updates">4. Fwupdate</a></b></li>
    <li><b><a href="#fwupd_updates">5. Fwupd</a></b></li>
    <li><b><a href="#key_updates">6. Sleutels</a></b></li>
  </ul></li>
  <li><b><a href="#buster_point_release">Debian tussenrelease 10.10 (<q>buster</q>)
        met bijgewerkte installatie- en live-media</a></b></li>
  <li><b><a href="#more_info">Meer informatie</a></b></li>
</ul>

<h1><a name="what_is_SB">Achtergrond: Wat is UEFI Secure Boot?</a></h1>

<p>
UEFI Secure Boot (SB - UEFI veilige opstart) is een verificatiemechanisme
dat moet verzekeren dat de code die door de UEFI firmware van een computer
gelanceerd wordt, te vertrouwen is. Het is ontworpen om het systeem te
beschermen tegen het laden en uitvoeren van kwaadaardige code vroegtijdig in
het opstartproces, nog voordat het besturingssysteem geladen wordt.
</p>

<p>
SB werkt met cryptografische controlegetallen en handtekeningen. Elk programma
dat door de firmware geladen wordt heeft een handtekening en een controlegetal
en voordat de uitvoering van het programma toegestaan wordt, zal de firmware
nagaan of het betrouwbaar is door de geldigheid van het controlegetal en de
handtekening te controleren. Wanneer SB op het systeem geactiveerd is, zal een
poging om een onbetrouwbaar programma uit te voeren niet toegestaan worden. Dit
voorkomt het uitvoeren van onverwachte/ongeoorloofde code in een UEFI-omgeving.
</p>

<p>
De meeste x86-hardware wordt door de fabrikant geleverd met Microsoft-sleutels.
Dit betekent dat de firmware op deze systemen binaire code zal vertrouwen die
ondertekend werd door Microsoft. De meeste moderne systemen worden geleverd met
een geactiveerd SB-systeem - standaard zullen zij geen niet-ondertekende code
uitvoeren, maar het is mogelijk om de firmware-configuratie aan te passen om SB
uit te schakelen of om extra ondertekeningssleutels op te nemen.
</p>

<p>
Zoals veel op Linux gebaseerde besturingssystemen, gebruikt Debian het
programma shim om dat vertrouwen van de firmware uit te breiden naar de andere
programma's die beveiligd moeten worden tijdens de vroege stadia van het
opstartproces: de GRUB2-bootloader, de Linux-kernel en hulpmiddelen voor het
opwaarderen van firmware (fwupd en fwupdate).
</p>

<h1><a name="grub_bugs">Meerdere bugs aangetroffen in GRUB2</a></h1>

<p>
Er werd een bug aangetroffen in de module <q>acpi</q> van GRUB2. Deze module is
ontworpen om een stuurprogramma-interface te bieden voor ACPI ("Advanced
Configuration and Power Interface" - Geavanceerde configuratie en
elektrische voedingsinterface), een gebruikelijk onderdeel van moderne
computerhardware. Helaas staat de ACPI-module momenteel ook een bevoorrechte
gebruiker toe om vervaardigde ACPI-tabellen onder Secure Boot te laden en
willekeurige wijzigingen in de systeemstatus aan te brengen; hierdoor kunnen
mensen gemakkelijk de Secure Boot-keten doorbreken. Dit beveiligingslek is nu
verholpen.
</p>

<p>
Zoals dit het geval was met BootHole, hebben ontwikkelaars in plaats van enkel
deze ene bug te repareren een diepgaande audit en analyse van de broncode van
GRUB2 uitgevoerd. Het ware onverantwoord geweest om enkel één belangrijk
probleem op te lossen zonder ook op zoek te gaan naar mogelijke andere
problemen. We hebben een paar andere plaatsen gevonden waar interne
geheugentoewijzingen kunnen overlopen bij onverwachte invoer en enkele plaatsen
waar geheugen kan worden gebruikt nadat het is vrijgegeven. Oplossingen voor al
deze problemen werden binnen de gemeenschap uitgewisseld en getest.
</p>

<p>
Nogmaals, zie het <a href="$(HOME)/security/2021/dsa-4867">Debian
Beveiligingsadvies 4867-1</a> voor een volledige lijst van gevonden problemen.
</p>


<h1><a name="revocations">Er moeten sleutels ingetrokken worden om de
Secure-Boot-keten te repareren</a></h1>

<p>
Debian en andere leveranciers van besturingssystemen zullen ongetwijfeld
<a href="#package_updates">gerepareerde versies uitbrengen</a> van GRUB2. Dit
houdt echter geen volledige reparatie in van de hier gesignaleerde
problemen. Kwaadwillige personen zouden nog steeds gebruik kunnen maken van
oudere kwetsbare versies van GRUB2 om Secure Boot te omzeilen.
</p>

<p>
Om dat te stoppen, is de volgende stap dat Microsoft die onveilige binaire
bestanden blokkeert om te voorkomen dat ze onder SB worden uitgevoerd. Dit
is mogelijk via de <b>DBX</b>-lijst, een functionaliteit van het UEFI Secure
Boot-ontwerp. Aan alle Linux-distributies die door Microsoft ondertekende
versies van shim bevatten, werd gevraagd details over te maken van de
betreffende programma's en sleutels om dit proces te faciliteren. Het <a
href="https://uefi.org/revocationlistfile">bestand met de lijst van
UEFI-intrekkingen</a> zal bijgewerkt worden om deze informatie op te nemen. Op
<b>een bepaald</b> moment in de toekomst zullen systemen deze bijgewerkte lijst
beginnen gebruiken en weigeren de kwetsbare programma's nog langer onder Secure
Boot uit te voeren.
</p>

<p>
De <i>exacte</i> tijdlijn voor het ontplooien van deze aanpassingen is nog niet
bekend. Ergens in de toekomst zullen BIOS/UEFI-leveranciers deze nieuwe lijst
met intrekkingen beginnen gebruiken in recentere firmware-versies voor nieuwe
hardware. <b>Mogelijk</b> zal ook Microsoft updates uitbrengen voor bestaande
systemen via Windows Update. Mogelijk zullen bepaalde Linux-distributies
updates uitbrengen via hun eigen proces van beveiligingsupdates.
<b>Momenteel</b> doet Debian dat nog niet, maar we bekijken het voor de
toekomst.
</p>

<h1><a name="revocation_problem">Welke effecten heeft het intrekken van de
sleutels?</a></h1>

<p>
De meeste leveranciers wantrouwen het automatisch toepassen van updates die
sleutels intrekken die voor Secure Boot worden gebruikt. Voor SB geschikt
gemaakte bestaande software-installaties zouden mogelijk plots helemaal niet
meer kunnen opstarten, tenzij de gebruiker er zorg voor gedragen heeft om ook
alle nodige software-updates te installeren. Windows/Linux dual-bootsystemen
zouden plots niet meer in staat kunnen zijn om Linux op te starten. Ook oude
installaties en live-systemen zouden natuurlijk niet langer kunnen opstarten,
wat het potentieel moeilijker zou maken om systemen te herstellen.

</p>

<p>
Er zijn twee voor de hand liggende manieren om een dergelijk niet-opstartend
systeem te repareren:
</p>

<ul>
  <li>Herstarten in <q>rescue</q>-modus (reparatiemodus)
    met <a href="#buster_point_release">recentere installatiemedia</a> en
    de noodzakelijke updates op die manier toepassen; of</li>
  <li>Secure Boot tijdelijk uitzetten om opnieuw toegang te krijgen tot
    het systeem, de updates toepassen en dan Secure Boot opnieuw aanzetten.</li>
</ul>

<p>
Beide mogelijkheden kunnen eenvoudig lijken, maar allebei kunnen ze tijdrovend
zijn voor gebruikers die verschillende systemen moeten onderhouden. Houd er ook
rekening mee dat het in- of uitschakelen van Secure Boot standaard directe
toegang tot de machine vereist. Normaal is het <b>onmogelijk</b> om
deze configuratie aan te passen buiten het instellingenprogramma van de
firmware om. Net om deze reden dient men mogelijk extra zorg te besteden aan
servercomputers die op een andere locatie staan.
</p>

<p>
Om deze redenen wordt <b>alle</b> Debian-gebruikers ten zeerste aanbevolen
ervoor te zorgen dat alle <a href="#package_updates">aanbevolen updates</a> zo
snel mogelijk op hun systeem geïnstalleerd worden om de kans op problemen in de
toekomst te verkleinen.
</p>

<h1><a name="package_updates">Bijgewerkte pakketten en sleutels</a></h1>

<p>
<b>Opmerking:</b> Systemen met Debian 9 (<q>stretch</q>) of ouder
zullen hier <b>niet</b> noodzakelijk updates krijgen, omdat Debian 10
(<q>buster</q>) de eerste release van Debian was met ingebouwde ondersteuning
voor UEFI Secure Boot.
</p>

<p>
Er zijn vijf broncodepakketten in Debian die zullen worden bijgewerkt vanwege
de hier beschreven UEFI Secure Boot-wijzigingen:
</p>

<h2><a name="grub_updates">1. GRUB2</a></h2>

<p>
Bijgewerkte versies van de GRUB2-pakketten van Debian zijn nu beschikbaar via
het archief debian-security van de stabiele release Debian 10
(<q>buster</q>). Zeer binnenkort zullen gerepareerde versies voor de
ontwikkelingsversies van Debian (unstable en testing) beschikbaar zijn
in het normale Debian archief.
</p>

<h2><a name="linux_updates">2. Linux</a></h2>

<p>
Bijgewerkte versies van de linux-pakketten van Debian zullen binnenkort
beschikbaar zijn via het archief buster-proposed-updates van de stabiele
release Debian 10 (<q>buster</q>) en zullen worden opgenomen in de komende
tussenrelease 10.10. Ook in het Debian-archief voor de ontwikkelingsversies van
Debian (unstable en testing) zullen binnenkort nieuwe pakketten aanwezig zijn.
We hopen tevens binnenkort gerepareerde pakketten te kunnen uploaden naar
buster-backports.
</p>

<h2><a name="shim_updates">3. Shim en SBAT</a></h2>

<p>
Met de reeks "BootHole"-bugs was het de eerste keer dat er op grote schaal
sleutels moesten worden ingetrokken in het UEFI Secure Boot-ecosysteem. Het
toonde een ongelukkige ontwerpfout in de intrekking van SB-sleutels: met veel
verschillende Linux-distributies en veel UEFI-binaire bestanden, groeit de
omvang van de intrekkingslijst snel. Veel computersystemen hebben slechts een
beperkte hoeveelheid ruimte voor het opslaan van gegevens over het intrekken
van sleutels, en die zou snel kunnen vollopen en dat systeem op verschillende
manieren onbruikbaar kunnen maken.
</p>

<p>
Om dit probleem aan te pakken, hebben de ontwikkelaars van shim een methode
bedacht om in de toekomst onveilige UEFI binaire bestanden te blokkeren die
veel meer ruimtebesparend is. Deze methode heet <b>SBAT</b> (<q>Secure Boot
Advanced Targeting</q> - Secure Boot geavanceerde doelgerichtheid). Ze werkt
door het bijhouden van generatienummers van ondertekende programma's. In plaats
van handtekeningen afzonderlijk in te trekken als er problemen worden gevonden,
worden tellers gebruikt om aan te geven dat oude versies van programma's niet
langer als veilig worden beschouwd. Het intrekken van een oude serie van GRUB2
binaire bestanden (bijvoorbeeld) wordt nu een geval van het bijwerken van een
UEFI-variabele die het generatienummer voor GRUB2 bevat; alle versies van
GRUB2-software die ouder zijn dan dat nummer zullen niet langer als veilig
worden beschouwd. Voor veel meer informatie over SBAT, zie de <a href="https://github.com/rhboot/shim/blob/main/SBAT.md">SBAT-documentatie</a> van shim.
</p>

<p>
<b>Helaas</b> is deze nieuwe SBAT-ontwikkeling voor shim nog niet helemaal
klaar. Ontwikkelaars waren van plan om nu een nieuwe versie van shim uit te
brengen met deze belangrijke nieuwe functie, maar stuitten op onverwachte
problemen. De ontwikkeling is nog steeds aan de gang. In de hele
Linux-gemeenschap verwachten we zeer binnenkort naar deze nieuwe versie van
shim te kunnen updaten. Totdat die klaar is, zullen we allemaal onze bestaande
ondertekende binaire bestanden van shim blijven gebruiken.
</p>

<p>
Bijgewerkte versies van Debian's shim-pakketten zullen beschikbaar worden
gesteld zodra dit werk is voltooid. Ze zullen hier en elders worden
aangekondigd. We zullen op dat moment een nieuwe tussenrelease 10.10 publiceren,
en ook nieuwe shim-pakketten uitbrengen voor de ontwikkelingsversies van Debian
(unstable en testing).
</p>

<h2><a name="fwupdate_updates">4. Fwupdate</a></h2>

<p>
Bijgewerkte versies van de fwupdate-pakketten van Debian zullen binnenkort via
buster-proposed-updates beschikbaar zijn voor de stabiele release Debian 10
(<q>buster</q>) en deze zullen opgenomen worden in de komende tussenrelease
10.10. Reeds een tijd geleden werd fwupdate verwijderd uit unstable en testing
ten gunste van fwupd.
</p>

<h2><a name="fwupd_updates">5. Fwupd</a></h2>

<p>
Bijgewerkte versies van de fwupd-pakketten van Debian zullen binnenkort via
buster-proposed-updates beschikbaar zijn voor de stabiele release Debian 10
(<q>buster</q>) en deze zullen opgenomen worden in de komende tussenrelease
10.10. Er zijn ook nieuwe pakketten in het archief van Debian voor de
ontwikkelingsversies van Debian (unstable en testing).
</p>

<h2><a name="key_updates">6. Sleutels</a></h2>

<p>
Debian heeft nieuwe ondertekeningssleutels en certificaten gegenereerd voor zijn Secure Boot-pakketten. Vroeger gebruikten we één certificaat voor al onze pakketten:
</p>

<ul>
  <li>Debian Secure Boot Ondertekening 2020
  <ul>
    <li>(vingerafdruk <code>3a91a54f9f46a720fe5bbd2390538ba557da0c2ed5286f5351fe04fff254ec31)</code></li>
  </ul></li>
</ul>

<p>
Wij zijn nu overgeschakeld op het gebruik van afzonderlijke sleutels en
certificaten voor elk van de vijf verschillende broncodepakketten, om in de
toekomst meer flexibiliteit te hebben:
</p>

<ul>
  <li>Debian Secure Boot Ondertekening 2021 - fwupd
  <ul>
    <li>(vingerafdruk <code>309cf4b37d11af9dbf988b17dfa856443118a41395d094fa7acfe37bcd690e33</code>)</li>
  </ul></li>
  <li>Debian Secure Boot Ondertekening 2021 - fwupdate
  <ul>
    <li>(vingerafdruk <code>e3bd875aaac396020a1eb2a7e6e185dd4868fdf7e5d69b974215bd24cab04b5d</code>)</li>
  </ul></li>
  <li>Debian Secure Boot Ondertekening 2021 - grub2
  <ul>
    <li>(vingerafdruk <code>0ec31f19134e46a4ef928bd5f0c60ee52f6f817011b5880cb6c8ac953c23510c</code>)</li>
  </ul></li>
  <li>Debian Secure Boot Ondertekening 2021 - linux
  <ul>
    <li>(vingerafdruk <code>88ce3137175e3840b74356a8c3cae4bdd4af1b557a7367f6704ed8c2bd1fbf1d</code>)</li>
  </ul></li>
  <li>Debian Secure Boot Ondertekening 2021 - shim
  <ul>
    <li>(vingerafdruk <code>40eced276ab0a64fc369db1900bd15536a1fb7d6cc0969a0ea7c7594bb0b85e2</code>)</li>
  </ul></li>
</ul>

<h1><a name="buster_point_release">Tussenrelease Debian 10.10 (<q>buster</q>),
bijgewerkte installatie- en live-media</a></h1>

<p>
Het is de bedoeling dat alle hier beschreven oplossingen opgenomen worden in de
tussenrelease Debian 10.10 (<q>buster</q>), welke voor binnenkort voorzien is.
Om die reden zal 10.10 een goede keuze zijn voor gebruikers die op zoek zijn
naar Debian installatie- en live-media. Het is mogelijk dat eerdere versies
niet langer zullen werken met Secure Boot wanneer de intrekkingen van de
sleutels uitgevoerd worden.
</p>

<h1><a name="more_info">Meer informatie</a></h1>

<p>
Veel meer informatie over hoe UEFI Secure Boot opgezet is in Debian, is te
vinden in de wiki-pagina's van Debian - zie
<a href="https://wiki.debian.org/SecureBoot">https://wiki.debian.org/SecureBoot</a>.</p>

<p>
Andere informatiebronnen over dit onderwerp zijn onder meer:
</p>

<ul>
  <li><a href="https://access.redhat.com/security/vulnerabilities/RHSB-2021-003">Artikel van Red
  Hat over de kwetsbaarheid</a></li>
  <li><a href="https://www.suse.com/support/kb/doc/?id=000019892">Advies van
  SUSE</a></li>
  <li><a href="https://wiki.ubuntu.com/SecurityTeam/KnowledgeBase/GRUB2SecureBootBypass2021">Artikel van Ubuntu
  over beveiliging</a></li>
</ul>
